//package com.mega.gateway.controller;
//
//import java.util.HashMap;
//import java.util.LinkedHashMap;
//import java.util.Map;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RestController;
//
//import com.mega.gateway.model.Upload;
//import com.mega.gateway.service.UploadService;
//
//@RestController
//@RequestMapping("/upload")
//public class UploadController {
//
//	@Autowired
//	private UploadService uploadService;
//
//	@PostMapping("/save")
//	private ResponseEntity<?> saveUpload(@RequestBody LinkedHashMap<String, Object> data){
//		String name = data.get("name").toString();
//		String base64 = data.get("base64").toString();
//		
//		Upload upload = new Upload();
//		upload.setName(name);
//		upload.setBase64(base64);
//		
//		Upload uploadTemp = uploadService.save(upload);
//		
//		return new ResponseEntity<>(uploadTemp, HttpStatus.OK);
//	}
//	
//	@GetMapping("/findByName/{name}")
//	private ResponseEntity<?> findByName(@PathVariable("name") String name){
//		Upload upload = uploadService.findByName(name);
//		Map<String, String> mapBase64 = new HashMap<>();
//		mapBase64.put("base64", upload.getBase64());
//		
//		return new ResponseEntity<>(mapBase64, HttpStatus.OK);
//	}
//
//}
