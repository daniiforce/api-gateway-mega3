package com.mega.gateway.controller;

import java.util.LinkedHashMap;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mega.gateway.model.LoginDTO;
//import com.mega.gateway.model.User;
import com.mega.gateway.security.jwt.TokenProvider;
//import com.mega.gateway.service.UserService;

@RestController
@RequestMapping(value = "/auth")
public class AuthenticationResource {

    private final Logger logger = LoggerFactory.getLogger(AuthenticationResource.class);

    @Autowired
    private TokenProvider tokenProvider;

    @Autowired
    private AuthenticationManager authenticationManager;
    
//    @Autowired
//    private UserService userService;
    
    /**
     * Method for authenticating user.
     *
     * @param loginDTO - login transfer object.
     * @param httpServletResponse - http response object
     * @return Response entity object. String response.
     */
    @PostMapping(value = "/login", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> authorize(@Valid @RequestBody LoginDTO loginDTO,
                                       HttpServletResponse httpServletResponse)
    {
        logger.debug("Credentials: {}", loginDTO.toString());
        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken
                = new UsernamePasswordAuthenticationToken(loginDTO.getUsername(), loginDTO.getPassword());

        try
        {
        	Authentication authentication = this.authenticationManager.authenticate(usernamePasswordAuthenticationToken);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            boolean rememberMe = (loginDTO.getRememberMe() == null) ? false : loginDTO.getRememberMe();

            String token = tokenProvider.createToken(authentication, rememberMe);
            httpServletResponse.addHeader("Authorization", "Bearer " + token);
            
//            Optional<User> userOptional = userService.findByUsername(loginDTO.getUsername());
//            User user = userOptional.get();

            LinkedHashMap<String, Object> responseLogin = new LinkedHashMap<>();
            responseLogin.put("token", token);
            responseLogin.put("user", loginDTO.getUsername());
            
            return ResponseEntity.ok(responseLogin);
        }
        catch (AuthenticationException e) {
            return new ResponseEntity<>(e.getLocalizedMessage(), HttpStatus.UNAUTHORIZED);
        }
    }
}
