package com.mega.gateway.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mega.gateway.service.TaskService;

@RestController
@RequestMapping("/task")
public class TaskController {

	private final TaskService service;
	
	@Autowired
	public TaskController(TaskService service) {
		this.service = service;
	}
	
	@PostMapping("/submit_task")
	public ResponseEntity<?> submitTask() {
		Map<String, Object> mapProcess = service.listProcess();
		List<Map<String, Object>> listProcess = (List<Map<String, Object>>) mapProcess.get("processes");
		Map<String, Object> getProcess = listProcess.get(0);
		
		int i = 0;
		if(getProcess.get("process-id").toString() != null && !getProcess.get("process-id").toString().equalsIgnoreCase("")) {
			i = service.submitTask(getProcess.get("process-id").toString());
		}
		return new ResponseEntity<>(i, HttpStatus.OK);
	}
}