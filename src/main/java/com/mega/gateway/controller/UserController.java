//package com.mega.gateway.controller;
//
//import java.util.Optional;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RestController;
//
//import com.mega.gateway.model.User;
//import com.mega.gateway.service.UserService;
//
//@RestController
//@RequestMapping("/user")
//public class UserController {
//
//	@Autowired
//	private UserService userService;
//	
//	@GetMapping("/findByUsername")
//	private Optional<User> findByUsername(@RequestParam String username) {
//		Optional<User> user = userService.findByUsername(username);
//		return user;
//	}
//
//	@GetMapping("/findByUsernameOption")
//	private Optional<User> findByUsernameOption(@RequestParam String username) {
//		Optional<User> user = userService.findByUsernameOption(username);
//		return user;
//	}
//
//}
