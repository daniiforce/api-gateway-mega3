package com.mega.gateway.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mega.gateway.model.DataType;
import com.mega.gateway.service.DataTypeInterfaceService;

@RestController
@RequestMapping("/data_type")
public class DataTypeController {

	private final DataTypeInterfaceService dataTypeInterfaceService;

	@Autowired
	public DataTypeController(DataTypeInterfaceService dataTypeInterfaceService) {
		this.dataTypeInterfaceService = dataTypeInterfaceService;
	}

	@GetMapping("/findAll")
	public ResponseEntity<?> findAll() {
		List<DataType> datatypes = new ArrayList<>();
		try {
			datatypes = dataTypeInterfaceService.findAll();
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(datatypes, HttpStatus.OK);
	}
	
	@GetMapping("/findByCode/{code}")
	public ResponseEntity<?> findByCode(@PathVariable("code") String code) {
		DataType dataType = null;
		try {
			dataType = dataTypeInterfaceService.findByCode(code);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(dataType, HttpStatus.OK);
	}

}