package com.mega.gateway.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mega.gateway.service.ProcessService;

@RestController
@RequestMapping("/process")
public class ProcessController {

	private final ProcessService service;
	
	@Autowired
	public ProcessController(ProcessService service) {
		this.service = service;
	}
	
	@GetMapping("/listProcess")
	public ResponseEntity<?> listProcess() {
		Map<String, Object> mapProcess = service.listProcess();
		return new ResponseEntity<>(mapProcess, HttpStatus.OK);		
	}
	
	@GetMapping("/workItem/{instanceId}")
	public ResponseEntity<?> workItem(@PathVariable("instanceId") String instanceId) {
		Map<String, Object> mapWorkItem = service.listWorkItems(Integer.valueOf(instanceId));
		return new ResponseEntity<>(mapWorkItem, HttpStatus.OK);		
	}

}