package com.mega.gateway.controller;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mega.gateway.model.Reward;
import com.mega.gateway.service.RewardInterfaceService;
import com.mega.gateway.service.RewardService;

@RestController
@RequestMapping("/reward")
public class RewardController {

	@Autowired
	private RewardInterfaceService rewardInterfaceService;

	private final RewardService rewardService;

	@Autowired
	public RewardController(RewardService rewardService) {
		this.rewardService = rewardService;
	}

	@PostMapping("/submitReward")
	public ResponseEntity<?> submitReward(@RequestBody LinkedHashMap<String, Object> data) {
		String process_id = data.get("process_id").toString();

		ObjectMapper objectMapper = new ObjectMapper();
		Reward reward = objectMapper.convertValue(data.get("reward"), Reward.class);

		String username = data.get("username").toString();

		int instanceId = rewardService.submitReward(process_id, reward, username);
		return new ResponseEntity<>(instanceId, HttpStatus.OK);
	}

	@PostMapping("/save")
	public ResponseEntity<?> saveReward(@RequestBody Reward reward) {
		Reward reward2 = new Reward();
		try {
			reward2 = rewardInterfaceService.save(reward);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(reward2, HttpStatus.OK);
	}

	@GetMapping("/findByEmpId/{empId}")
	public ResponseEntity<?> findByEmpId(@PathVariable String empId) {
		Reward reward2 = new Reward();
		try {
			reward2 = rewardInterfaceService.findByEmpId(empId);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(reward2, HttpStatus.OK);
	}

	@PostMapping(value = "/edit/{code}")
	public ResponseEntity<Reward> rewardEdit(@PathVariable("code") String empId, @RequestBody Reward reward) {

		Reward modifyReward = new Reward();
		try {
//			modifyReward = rewardInterfaceService.findByEmpId(empId);

			modifyReward = rewardInterfaceService.findByEmpIdAndStartDateAndEndDateAndSeq(reward.getEmpId(), reward.getStartDate(), reward.getEndDate(), reward.getSeq());
			
			modifyReward.setAmount(reward.getAmount() != null ? reward.getAmount() : "");
			modifyReward.setLandscape(reward.getLandscape() != null ? reward.getLandscape() : "");
			modifyReward.setEmpId(reward.getEmpId() != null ? reward.getEmpId() : "");
			modifyReward.setStartDate(reward.getStartDate() != null ? reward.getStartDate() : "");
			modifyReward.setEndDate(reward.getEndDate() != null ? reward.getEndDate() : "");
			modifyReward.setSeq(reward.getSeq() != null ? reward.getSeq() : "");
			modifyReward.setData_type(reward.getData_type() != null ? reward.getData_type() : "");
			modifyReward.setDescription(reward.getDescription() != null ? reward.getDescription() : "");
			modifyReward.setResponsible(reward.getResponsible() != null ? reward.getResponsible() : "");
			modifyReward.setUser_change(reward.getUser_change() != null ? reward.getUser_change() : "");
			modifyReward.setLast_change(reward.getLast_change() != null ? reward.getLast_change() : "");
			modifyReward.setCreated_by(reward.getCreated_by() != null ? reward.getCreated_by() : "");
			modifyReward.setCreated_date(reward.getCreated_date() != null ? reward.getCreated_date() : "");
			modifyReward.setNo_sk(reward.getNo_sk() != null ? reward.getNo_sk() : "");
			modifyReward.setEffective_date(reward.getEffective_date() != null ? reward.getEffective_date() : "");

			rewardInterfaceService.save(modifyReward);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(modifyReward, HttpStatus.OK);
	}
	
	@PostMapping("/findByAllId")
	public ResponseEntity<?> findByAllId(@RequestBody LinkedHashMap<String, Object> data) {
		Reward reward2 = new Reward();
		try {
			reward2 = rewardInterfaceService.findByEmpIdAndStartDateAndEndDateAndSeq(data.get("empId").toString(), 
					data.get("startDate").toString(), data.get("endDate").toString(), data.get("seq").toString());
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(reward2, HttpStatus.OK);
	}
	
	@GetMapping("/findAllByEmpId/{empId}")
	public ResponseEntity<?> findAllByEmpId(@PathVariable("empId") String empId) {
		List<Reward> rewards = new ArrayList<>();
		try {
			rewards = rewardInterfaceService.findByEmpIdEquals(empId);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(rewards, HttpStatus.OK);
	}

}