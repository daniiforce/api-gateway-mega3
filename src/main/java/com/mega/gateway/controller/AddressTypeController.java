package com.mega.gateway.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mega.gateway.model.AddressType;
import com.mega.gateway.service.AddressTypeInterfaceService;

@RestController
@RequestMapping("/address_type")
public class AddressTypeController {

	private final AddressTypeInterfaceService addressTypeInterfaceService;

	@Autowired
	public AddressTypeController(AddressTypeInterfaceService addressTypeInterfaceService) {
		this.addressTypeInterfaceService = addressTypeInterfaceService;
	}
	
	@GetMapping("/findAll")
	public ResponseEntity<?> findAll() {
		List<AddressType> addressTypes = new ArrayList<>();
		try {
			addressTypes = addressTypeInterfaceService.findAll();
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(addressTypes, HttpStatus.OK);
	}
	
	@GetMapping("/findByCode/{code}")
	public ResponseEntity<?> findByCode(@PathVariable("code") String code) {
		AddressType addressType = null;
		try {
			addressType = addressTypeInterfaceService.findByCode(code);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(addressType, HttpStatus.OK);
	}
	
}