package com.mega.gateway.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mega.gateway.model.LdapTesting;
import com.mega.gateway.service.LdapService;

@RestController
@RequestMapping("/ldap")
public class LdapTestingController {
	
	@Autowired
	private LdapService ldapService;
	
	@GetMapping("/search")
	public LdapTesting testingLdap(@RequestParam String operation, @RequestParam String id) {
		LdapTesting ldapTesting = ldapService.testing(operation, id); 
		return ldapTesting;
	}

	@GetMapping("/verify")
	public void verifyLdap(@RequestParam String operation, @RequestParam String id, @RequestParam String password) {
		Map<String, String> verifyUserPassword = ldapService.verifyUserPassword(operation, id, password); 
		String tempResponseCode = verifyUserPassword.get("ResponseCode");
		String tempResponseDescription = verifyUserPassword.get("ResponseDescription");
	}

}
