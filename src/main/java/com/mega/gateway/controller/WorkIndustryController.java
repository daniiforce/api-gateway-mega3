package com.mega.gateway.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mega.gateway.model.WorkIndustry;
import com.mega.gateway.service.WorkIndustryInterfaceService;

@RestController
@RequestMapping("/industry")
public class WorkIndustryController {

	private final WorkIndustryInterfaceService workIndustryInterfaceService;

	@Autowired
	public WorkIndustryController(WorkIndustryInterfaceService workIndustryInterfaceService) {
		this.workIndustryInterfaceService = workIndustryInterfaceService;
	}

	@GetMapping("/findAll")
	public ResponseEntity<?> findAll() {
		List<WorkIndustry> industries = new ArrayList<>();
		try {
			industries = workIndustryInterfaceService.findAll();
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(industries, HttpStatus.OK);
	}
	
	@GetMapping("/findByCode/{code}")
	public ResponseEntity<?> findByCode(@PathVariable("code") String code) {
		WorkIndustry workIndustry = null;
		try {
			workIndustry = workIndustryInterfaceService.findByCode(code);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(workIndustry, HttpStatus.OK);
	}

}