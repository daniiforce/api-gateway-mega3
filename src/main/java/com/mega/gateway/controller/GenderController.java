package com.mega.gateway.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mega.gateway.model.Gender;
import com.mega.gateway.service.GenderInterfaceService;

@RestController
@RequestMapping("/gender")
public class GenderController {

	private final GenderInterfaceService genderInterfaceService;

	@Autowired
	public GenderController(GenderInterfaceService genderInterfaceService) {
		this.genderInterfaceService = genderInterfaceService;
	}

	@GetMapping("/findAll")
	public ResponseEntity<?> findAll() {
		List<Gender> genders = new ArrayList<>();
		try {
			genders = genderInterfaceService.findAll();
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(genders, HttpStatus.OK);
	}

}