package com.mega.gateway.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mega.gateway.model.Province;
import com.mega.gateway.service.ProvinceInterfaceService;

@RestController
@RequestMapping("/province")
public class ProvinceController {

	private final ProvinceInterfaceService provinceInterfaceService;

	@Autowired
	public ProvinceController(ProvinceInterfaceService provinceInterfaceService) {
		this.provinceInterfaceService = provinceInterfaceService;
	}
	
	@GetMapping("/findAll")
	public ResponseEntity<?> findAll() {
		List<Province> provinces = new ArrayList<>();
		try {
			provinces = provinceInterfaceService.findAll();
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(provinces, HttpStatus.OK);
	}
	
	@GetMapping("/findByCode/{code}")
	public ResponseEntity<?> findByCode(@PathVariable("code") String code) {
		Province province = null;
		try {
			province = provinceInterfaceService.findByCode(code);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(province, HttpStatus.OK);
	}
	
}