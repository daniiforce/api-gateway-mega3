package com.mega.gateway.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mega.gateway.model.FamilyType;
import com.mega.gateway.service.FamilyTypeInterfaceService;

@RestController
@RequestMapping("/family_type")
public class FamilyTypeController {

	private final FamilyTypeInterfaceService familyTypeInterfaceService;

	@Autowired
	public FamilyTypeController(FamilyTypeInterfaceService familyTypeInterfaceService) {
		this.familyTypeInterfaceService = familyTypeInterfaceService;
	}

	@GetMapping("/findAll")
	public ResponseEntity<?> findAll() {
		List<FamilyType> familytypes = new ArrayList<>();
		try {
			familytypes = familyTypeInterfaceService.findAll();
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(familytypes, HttpStatus.OK);
	}

}