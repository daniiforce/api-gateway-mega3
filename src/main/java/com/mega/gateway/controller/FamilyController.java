package com.mega.gateway.controller;

import java.util.LinkedHashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mega.gateway.model.Family;
import com.mega.gateway.service.FamilyInterfaceService;
import com.mega.gateway.service.FamilyService;

@RestController
@RequestMapping("/family")
public class FamilyController {

	@Autowired
	private FamilyInterfaceService familyInterfaceService;

	private final FamilyService familyService;

	@Autowired
	public FamilyController(FamilyService familyService) {
		this.familyService = familyService;
	}

	@PostMapping("/submitFamily")
	public ResponseEntity<?> submitFamily(@RequestBody LinkedHashMap<String, Object> data) {
		String process_id = data.get("process_id").toString();

		ObjectMapper objectMapper = new ObjectMapper();
		Family family = objectMapper.convertValue(data.get("family"), Family.class);

		String username = data.get("username").toString();

		int instanceId = familyService.submitFamily(process_id, family, username);
		return new ResponseEntity<>(instanceId, HttpStatus.OK);
	}

	@PostMapping("/save")
	public ResponseEntity<?> saveFamily(@RequestBody Family family) {
		Family family2 = new Family();
		try {
			family2 = familyInterfaceService.save(family);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(family2, HttpStatus.OK);
	}

	@GetMapping("/findByEmpId/{empId}")
	public ResponseEntity<?> findByEmpId(@PathVariable String empId) {
		Family family2 = new Family();
		try {
			family2 = familyInterfaceService.findByEmpId(empId);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(family2, HttpStatus.OK);
	}

	@PostMapping(value = "/edit/{code}")
	public ResponseEntity<Family> familyEdit(@PathVariable("code") String empId, @RequestBody Family family) {

		Family modifyFamily = new Family();
		try {
			modifyFamily = familyInterfaceService.findByEmpId(empId);

			modifyFamily.setLandscape(family.getLandscape() != null ? family.getLandscape() : "");
			modifyFamily.setEmpId(family.getEmpId() != null ? family.getEmpId() : "");
			modifyFamily.setStart_date(family.getStart_date() != null ? family.getStart_date() : "");
			modifyFamily.setEnd_date(family.getEnd_date() != null ? family.getEnd_date() : "");
			modifyFamily.setSeq(family.getSeq() != null ? family.getSeq() : "");
			modifyFamily.setFamily_type(family.getFamily_type() != null ? family.getFamily_type() : "");
			modifyFamily.setName(family.getName() != null ? family.getName() : "");
			modifyFamily.setGender(family.getGender() != null ? family.getGender() : "");
			modifyFamily.setBirth_date(family.getBirth_date() != null ? family.getBirth_date() : "");
			modifyFamily.setBirth_place(family.getBirth_place() != null ? family.getBirth_place() : "");
			modifyFamily.setDate_of_death(family.getDate_of_death() != null ? family.getDate_of_death() : "");
			modifyFamily.setLast_education(family.getLast_education() != null ? family.getLast_education() : "");
			modifyFamily.setAddress(family.getAddress() != null ? family.getAddress() : "");
			modifyFamily.setTelp_home(family.getTelp_home() != null ? family.getTelp_home() : "");
			modifyFamily.setTelp_office(family.getTelp_office() != null ? family.getTelp_office() : "");
			modifyFamily.setOccupation(family.getOccupation() != null ? family.getOccupation() : "");
			modifyFamily.setMedical_flag(family.getMedical_flag() != null ? family.getMedical_flag() : "");
			modifyFamily.setNo_surat(family.getNo_surat() != null ? family.getNo_surat() : "");
			modifyFamily.setFamily_medical_code(family.getFamily_medical_code() != null ? family.getFamily_medical_code() : "");
			modifyFamily.setNo_asuransi(family.getNo_asuransi() != null ? family.getNo_asuransi() : "");
			modifyFamily.setNo_bpjs(family.getNo_bpjs() != null ? family.getNo_bpjs() : "");
			modifyFamily.setNo_cug(family.getNo_cug() != null ? family.getNo_cug() : "");
			modifyFamily.setUser_change(family.getUser_change() != null ? family.getUser_change() : "");
			modifyFamily.setLast_change(family.getLast_change() != null ? family.getLast_change() : "");
			modifyFamily.setCreated_by(family.getCreated_by() != null ? family.getCreated_by() : "");
			modifyFamily.setCreated_date(family.getCreated_date() != null ? family.getCreated_date() : "");
			
			familyInterfaceService.save(modifyFamily);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(modifyFamily, HttpStatus.OK);
	}

}