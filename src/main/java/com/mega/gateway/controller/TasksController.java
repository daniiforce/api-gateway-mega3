package com.mega.gateway.controller;

import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mega.gateway.service.TasksService;

@RestController
@RequestMapping("/tasks")
public class TasksController {
	
	private final TasksService service;
	
	@Autowired
	public TasksController(TasksService service) {
		this.service = service;
	}
	
	@PostMapping("/ownerTasks")
	public ResponseEntity<?> ownerTasks(@RequestBody LinkedHashMap<String, Object> data){
		String username = data.get("username").toString();
		String password = data.get("password").toString();
		
		Map<String, Object> ownerTasks = service.ownerTasks(username, password);
		return new ResponseEntity<>(ownerTasks, HttpStatus.OK);
	}
	
	@PostMapping("/getTask")
	public ResponseEntity<?> getTask(@RequestBody LinkedHashMap<String, Object> data){
		String username = data.get("username").toString();
		String password = data.get("password").toString();
		String taskId = data.get("taskId").toString();
		
		Map<String, Object> ownerTasks = service.getTask(username, password, taskId);
		return new ResponseEntity<>(ownerTasks, HttpStatus.OK);
	}
	
	@PostMapping("/startCompleteTask")
	public ResponseEntity<?> startCompleteTask(@RequestBody LinkedHashMap<String, Object> data){
		String username = data.get("username").toString();
		String password = data.get("password").toString();
		String taskId = data.get("taskId").toString();
		
		int statusCode = service.startCompleteTask(username, password, taskId, data);
		return new ResponseEntity<>(statusCode, HttpStatus.OK);
	}

}