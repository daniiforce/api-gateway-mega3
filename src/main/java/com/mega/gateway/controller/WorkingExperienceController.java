package com.mega.gateway.controller;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mega.gateway.model.WorkingExperience;
import com.mega.gateway.service.WorkingExperienceInterfaceService;
import com.mega.gateway.service.WorkingExperienceService;

@RestController
@RequestMapping("/working_experience")
public class WorkingExperienceController {

	@Autowired
	private WorkingExperienceInterfaceService workingExperienceInterfaceService;

	private final WorkingExperienceService workingExperienceService;

	@Autowired
	public WorkingExperienceController(WorkingExperienceService workingExperienceService) {
		this.workingExperienceService = workingExperienceService;
	}

	@PostMapping("/submitWorkingExperience")
	public ResponseEntity<?> submitWorkingExperience(@RequestBody LinkedHashMap<String, Object> data) {
		String process_id = data.get("process_id").toString();

		ObjectMapper objectMapper = new ObjectMapper();
		WorkingExperience workingExperience = objectMapper.convertValue(data.get("workingExperience"), WorkingExperience.class);

		String username = data.get("username").toString();

		int instanceId = workingExperienceService.submitWorkingExperience(process_id, workingExperience, username);
		return new ResponseEntity<>(instanceId, HttpStatus.OK);
	}

	@PostMapping("/save")
	public ResponseEntity<?> saveWorkingExperience(@RequestBody WorkingExperience workingExperience) {
		WorkingExperience workingExperience2 = new WorkingExperience();
		try {
			workingExperience2 = workingExperienceInterfaceService.save(workingExperience);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(workingExperience2, HttpStatus.OK);
	}

	@GetMapping("/findByEmpId/{empId}")
	public ResponseEntity<?> findByEmpId(@PathVariable String empId) {
		WorkingExperience workingExperience2 = new WorkingExperience();
		try {
			workingExperience2 = workingExperienceInterfaceService.findByEmpId(empId);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(workingExperience2, HttpStatus.OK);
	}

	@PostMapping(value = "/edit/{code}")
	public ResponseEntity<WorkingExperience> workingExperienceEdit(@PathVariable("code") String empId, @RequestBody WorkingExperience workingExperience) {

		WorkingExperience modifyWorkingExperience = new WorkingExperience();
		try {
			modifyWorkingExperience = workingExperienceInterfaceService.findByEmpId(empId);
			
			modifyWorkingExperience.setLandscape(workingExperience.getLandscape() != null ? workingExperience.getLandscape() : "");
			modifyWorkingExperience.setEmpId(workingExperience.getEmpId() != null ? workingExperience.getEmpId() : "");
			modifyWorkingExperience.setStartDate(workingExperience.getStartDate() != null ? workingExperience.getStartDate() : "");
			modifyWorkingExperience.setEndDate(workingExperience.getEndDate() != null ? workingExperience.getEndDate() : "");
			modifyWorkingExperience.setSeq(workingExperience.getSeq() != null ? workingExperience.getSeq() : "");
			modifyWorkingExperience.setCompany(workingExperience.getCompany() != null ? workingExperience.getCompany() : "");
			modifyWorkingExperience.setJob(workingExperience.getJob() != null ? workingExperience.getJob() : "");
			modifyWorkingExperience.setPosition(workingExperience.getPosition() != null ? workingExperience.getPosition() : "");
			modifyWorkingExperience.setLocation(workingExperience.getLocation() != null ? workingExperience.getLocation() : "");
			modifyWorkingExperience.setDescription(workingExperience.getDescription() != null ? workingExperience.getDescription() : "");
			modifyWorkingExperience.setReason_leaving(workingExperience.getReason_leaving() != null ? workingExperience.getReason_leaving() : "");
			modifyWorkingExperience.setIndustry(workingExperience.getIndustry() != null ? workingExperience.getIndustry() : "");
			modifyWorkingExperience.setPhone_number(workingExperience.getPhone_number() != null ? workingExperience.getPhone_number() : "");
			modifyWorkingExperience.setSupervisor_name(workingExperience.getSupervisor_name() != null ? workingExperience.getSupervisor_name() : "");
			modifyWorkingExperience.setSupervisor_position(workingExperience.getSupervisor_position() != null ? workingExperience.getSupervisor_position() : "");
			modifyWorkingExperience.setWorking_exp(workingExperience.getWorking_exp() != null ? workingExperience.getWorking_exp() : "");
			modifyWorkingExperience.setStatus_reference(workingExperience.getStatus_reference() != null ? workingExperience.getStatus_reference() : "");
			modifyWorkingExperience.setDoc_completion(workingExperience.getDoc_completion() != null ? workingExperience.getDoc_completion() : "");
			modifyWorkingExperience.setKompensasi_terakhir(workingExperience.getKompensasi_terakhir() != null ? workingExperience.getKompensasi_terakhir() : "");
			modifyWorkingExperience.setIkatan_dinas(workingExperience.getIkatan_dinas() != null ? workingExperience.getIkatan_dinas() : "");
			modifyWorkingExperience.setUser_change(workingExperience.getUser_change() != null ? workingExperience.getUser_change() : "");
			modifyWorkingExperience.setLast_change(workingExperience.getLast_change() != null ? workingExperience.getLast_change() : "");
			modifyWorkingExperience.setCreated_by(workingExperience.getCreated_by() != null ? workingExperience.getCreated_by() : "");
			modifyWorkingExperience.setCreated_date(workingExperience.getCreated_date() != null ? workingExperience.getCreated_date() : "");

			workingExperienceInterfaceService.save(modifyWorkingExperience);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(modifyWorkingExperience, HttpStatus.OK);
	}
	
	@PostMapping("/findByAllId")
	public ResponseEntity<?> findByAllId(@RequestBody LinkedHashMap<String, Object> data) {
		WorkingExperience workingExperience2 = new WorkingExperience();
		try {
			workingExperience2 = workingExperienceInterfaceService.findByEmpIdAndStartDateAndEndDateAndSeq(data.get("empId").toString(), 
					data.get("startDate").toString(), data.get("endDate").toString(), data.get("seq").toString());
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(workingExperience2, HttpStatus.OK);
	}
	
	@GetMapping("/findAllByEmpId/{empId}")
	public ResponseEntity<?> findAllByEmpId(@PathVariable("empId") String empId) {
		List<WorkingExperience> workingExperiences = new ArrayList<>();
		try {
			workingExperiences = workingExperienceInterfaceService.findByEmpIdEquals(empId);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(workingExperiences, HttpStatus.OK);
	}

}