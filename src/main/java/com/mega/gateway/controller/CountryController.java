package com.mega.gateway.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mega.gateway.model.Country;
import com.mega.gateway.service.CountryInterfaceService;

@RestController
@RequestMapping("/country")
public class CountryController {

	private final CountryInterfaceService countryInterfaceService;

	@Autowired
	public CountryController(CountryInterfaceService countryInterfaceService) {
		this.countryInterfaceService = countryInterfaceService;
	}
	
	@GetMapping("/findAll")
	public ResponseEntity<?> findAll() {
		List<Country> countries = new ArrayList<>();
		try {
			countries = countryInterfaceService.findAll();
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(countries, HttpStatus.OK);
	}
	
	@GetMapping("/findByCode/{code}")
	public ResponseEntity<?> findByCode(@PathVariable("code") String code) {
		Country country = null;
		try {
			country = countryInterfaceService.findByCountryId(code);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(country, HttpStatus.OK);
	}
	
}