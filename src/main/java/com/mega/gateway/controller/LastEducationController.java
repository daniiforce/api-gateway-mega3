package com.mega.gateway.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mega.gateway.model.LastEducation;
import com.mega.gateway.service.LastEducationInterfaceService;

@RestController
@RequestMapping("/last_education")
public class LastEducationController {

	private final LastEducationInterfaceService lastEducationInterfaceService;

	@Autowired
	public LastEducationController(LastEducationInterfaceService lastEducationInterfaceService) {
		this.lastEducationInterfaceService = lastEducationInterfaceService;
	}

	@GetMapping("/findAll")
	public ResponseEntity<?> findAll() {
		List<LastEducation> lastEducations = new ArrayList<>();
		try {
			lastEducations = lastEducationInterfaceService.findAll();
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(lastEducations, HttpStatus.OK);
	}

}