package com.mega.gateway.controller;

import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mega.gateway.model.Person;
import com.mega.gateway.service.PersonService;

@RestController
@RequestMapping("/person")
public class PersonController {

	private final PersonService personService;

	@Autowired
	public PersonController(PersonService personService) {
		this.personService = personService;
	}
	
	@PostMapping("/submitPerson")
	public ResponseEntity<?> submitPerson(@RequestBody LinkedHashMap<String, Object> data){
		String process_id = data.get("process_id").toString();

		Person person = new Person();
		Map<String, Object> mapPerson = (Map<String, Object>) data.get("person");
		person.setFirstName(mapPerson.get("firstName").toString());
		person.setLasteName(mapPerson.get("lasteName").toString());

		String username = data.get("username").toString();

		int instanceId = personService.submitPerson(process_id, person, username);
		
		return new ResponseEntity<>(instanceId, HttpStatus.OK);
	}
	
}