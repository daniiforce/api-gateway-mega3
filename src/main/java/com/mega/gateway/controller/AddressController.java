package com.mega.gateway.controller;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mega.gateway.model.Address;
import com.mega.gateway.service.AddressInterfaceService;
import com.mega.gateway.service.AddressService;

@RestController
@RequestMapping("/address")
public class AddressController {

	@Autowired
	private AddressInterfaceService addressInterfaceService;

	private final AddressService addressService;

	@Autowired
	public AddressController(AddressService addressService) {
		this.addressService = addressService;
	}

	@PostMapping("/submitAddress")
	public ResponseEntity<?> submitAddress(@RequestBody LinkedHashMap<String, Object> data) {
		String process_id = data.get("process_id").toString();
		
		ObjectMapper objectMapper = new ObjectMapper();
		Address address = objectMapper.convertValue(data.get("address"), Address.class);

		String username = data.get("username").toString();
		
		int instanceId = addressService.submitAddress(process_id, address, username);
		return new ResponseEntity<>(instanceId, HttpStatus.OK);
	}

	@PostMapping("/save")
	public ResponseEntity<?> saveAddress(@RequestBody Address address) {
		Address address2 = new Address();
		try {
			address2 = addressInterfaceService.save(address);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(address2, HttpStatus.OK);
	}
	
	@GetMapping("/findByEmpId/{empId}")
	public ResponseEntity<?> findByEmpId(@PathVariable String empId) {
		Address address2 = new Address();
		try {
			address2 = addressInterfaceService.findByEmpId(empId);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(address2, HttpStatus.OK);
	}
	
	@GetMapping("/findAll")
	public ResponseEntity<?> findAll() {
		List<Address> addresses = new ArrayList<>();
		try {
			addresses = addressInterfaceService.findAll();
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(addresses, HttpStatus.OK);
	}
	
	@GetMapping("/findAllByEmpId/{empId}")
	public ResponseEntity<?> findAllByEmpId(@PathVariable("empId") String empId) {
		List<Address> addresses = new ArrayList<>();
		try {
			addresses = addressInterfaceService.findByEmpIdEquals(empId);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(addresses, HttpStatus.OK);
	}
	
	@PostMapping("/findByAllId")
	public ResponseEntity<?> findByAllId(@RequestBody LinkedHashMap<String, Object> data) {
		Address address2 = new Address();
		try {
			address2 = addressInterfaceService.findByEmpIdAndStartDateAndEndDateAndAddressTypeAndSeq(data.get("empId").toString(), 
					data.get("startDate").toString(), data.get("endDate").toString(), data.get("addressType").toString(), data.get("seq").toString());
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(address2, HttpStatus.OK);
	}
	
	@PostMapping(value="/edit/{code}")
	public ResponseEntity<Address> addressEdit(@PathVariable("code") String empId, @RequestBody Address address) {
		
		Address modifyAddress = new Address();
		try {
//			modifyAddress = addressInterfaceService.findByEmpId(empId);
			modifyAddress = addressInterfaceService.findByEmpIdAndStartDateAndEndDateAndAddressTypeAndSeq(address.getEmpId(), address.getStartDate(), address.getEndDate(), address.getAddressType(), address.getSeq());
			modifyAddress.setAddressType(address.getAddressType());
			modifyAddress.setCellphone_num(address.getCellphone_num());
			modifyAddress.setCity(address.getCity());
			modifyAddress.setContact_person(address.getContact_person());
			modifyAddress.setCountry(address.getCountry());
			modifyAddress.setCreated_by(address.getCreated_by());
			modifyAddress.setCreated_date(address.getCreated_date());
			modifyAddress.setEmpId(address.getEmpId());
			modifyAddress.setEndDate(address.getEndDate());
			modifyAddress.setKecamatan(address.getKecamatan());
			modifyAddress.setKelurahan(address.getKelurahan());
			modifyAddress.setKepemilikan(address.getKepemilikan());
			modifyAddress.setLandscape(address.getLandscape());
			modifyAddress.setLast_change(address.getLast_change());
			modifyAddress.setLocation(address.getLocation());
			modifyAddress.setPostalcode(address.getPostalcode());
			modifyAddress.setProvince(address.getProvince());
			modifyAddress.setRt(address.getRt());
			modifyAddress.setRw(address.getRw());
			modifyAddress.setSeq(address.getSeq());
			modifyAddress.setStartDate(address.getStartDate());
			modifyAddress.setStreet(address.getStreet());
			modifyAddress.setTelp_num(address.getTelp_num());
			modifyAddress.setUser_change(address.getUser_change());
			
			addressInterfaceService.save(modifyAddress);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(modifyAddress, HttpStatus.OK);
	}

}