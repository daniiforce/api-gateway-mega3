package com.mega.gateway;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.client.RestTemplate;

//import com.mega.gateway.model.Role;
//import com.mega.gateway.model.User;
//import com.mega.gateway.repository.RoleRepository;
//import com.mega.gateway.repository.UserRepository;

@SpringBootApplication
@ComponentScan(basePackages = { 
		"com.mega.gateway.model", 
		"com.mega.gateway.repository",
		"com.mega.gateway.service",
		"com.mega.gateway.service.impl",
		"com.mega.gateway.controller",
		"com.mega.gateway.config",
		"com.mega.gateway.security",
		"com.mega.gateway.security.jwt"
})
@EntityScan(basePackages = "com.mega.gateway.model")
@EnableJpaRepositories(basePackages = { "com.mega.gateway.repository" })
public class ApiGatewayMegaApplication extends SpringBootServletInitializer 
{
//implements CommandLineRunner {
	
//	@Autowired
//	private UserRepository userRepository;
//	
//	@Autowired
//	private RoleRepository roleRepository;

	public static void main(String[] args) {
		SpringApplication.run(ApiGatewayMegaApplication.class, args);
	}

//	@Override
//	public void run(String... args) throws Exception {
//		userRepository.deleteAllInBatch();
//		roleRepository.deleteAllInBatch();
//
//		ArrayList<Role> roles = new ArrayList<Role>();
//		Role role = new Role("admin");
//		roles.add(role);
//		
//		ArrayList<String> tempRole = new ArrayList<>();
//		tempRole.add("admin");
//		
//		User user = new User("ORISYS02", "Z2Vnm1QqbyrExTlCvBBdmjVbJds6QSJPo6QI", tempRole, true);
//
//		roleRepository.saveAndFlush(role);
//		userRepository.saveAndFlush(user);
//		
//		ArrayList<Role> roles1 = new ArrayList<Role>();
//		Role role1 = new Role("admin");
//		roles1.add(role1);
//		
//		ArrayList<String> tempRole1 = new ArrayList<>();
//		tempRole1.add("admin");
//		
//		User user1 = new User("ORISYS03", "Z2Vnm1QqbyrExTlCvBBdmjVbJds6QSJPo6QI", tempRole1, true);
//
//		roleRepository.saveAndFlush(role1);
//		userRepository.saveAndFlush(user1);
//		
//		ArrayList<Role> roles2 = new ArrayList<Role>();
//		Role role2 = new Role("admin");
//		roles2.add(role2);
//		
//		ArrayList<String> tempRole2 = new ArrayList<>();
//		tempRole2.add("admin");
//		
//		User user2 = new User("ORISYS04", "Z2Vnm1QqbyrExTlCvBBdmjVbJds6QSJPo6QI", tempRole2, true);
//
//		roleRepository.saveAndFlush(role2);
//		userRepository.saveAndFlush(user2);
//	}
	
	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}

}
