package com.mega.gateway.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name = "base_cust_ref_countryid")
@IdClass(CountryId.class)
public class Country implements Serializable {

	private static final long serialVersionUID = -4937700303687470114L;

	@Id
	@Column(length = 3)
	private String landscape;

	@Id
	@Column(name = "country_id", length = 3)
	private String countryId;

	@Column(length = 40)
	private String name;

	public String getLandscape() {
		return landscape;
	}

	public void setLandscape(String landscape) {
		this.landscape = landscape;
	}

	public String getCountryId() {
		return countryId;
	}

	public void setCountryId(String countryId) {
		this.countryId = countryId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
