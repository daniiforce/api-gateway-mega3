package com.mega.gateway.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "hr_md_emp_md0010")
@IdClass(RewardId.class)
public class Reward implements Serializable {

	private static final long serialVersionUID = 1888300435017852654L;

	@Id
	@Column(length = 3)
	private String landscape;

	@Id
	@Column(name = "emp_id", length = 8)
	private String empId;

	@Id
	@Column(name = "start_date", length = 8)
	private String startDate;

	@Id
	@Column(name = "end_date", length = 8)
	private String endDate;

	@Id
	@Column(length = 4)
	private String seq;

	@Column(length = 2)
	private String data_type;

	@Column(length = 200)
	private String description;

	@Column(length = 50)
	private String responsible;

	@Column(length = 10)
	private String amount;

	@Column(length = 8)
	private String user_change;

	@Column(length = 14)
	private String last_change;

	@Column(length = 8)
	private String created_by;

	@Column(length = 14)
	private String created_date;

	@Column(length = 50)
	private String no_sk;

	@Column(length = 8)
	private String effective_date;

	// Temporary

	@Transient
	private String requestorId;

	@Transient
	private String requestorName;

	@Transient
	private String subject;

	@Transient
	private String descriptionTemp;

	@Transient
	private String employeeId;

	@Transient
	private String employeeName;

	@Transient
	private String comment;

	@Transient
	private String base64;

	@Transient
	private Date startDateTemp;

	@Transient
	private Date endDateTemp;

	@Transient
	private String dataTypeStr;

	public String getLandscape() {
		return landscape;
	}

	public void setLandscape(String landscape) {
		this.landscape = landscape;
	}

	public String getEmpId() {
		return empId;
	}

	public void setEmpId(String empId) {
		this.empId = empId;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getSeq() {
		return seq;
	}

	public void setSeq(String seq) {
		this.seq = seq;
	}

	public String getUser_change() {
		return user_change;
	}

	public void setUser_change(String user_change) {
		this.user_change = user_change;
	}

	public String getLast_change() {
		return last_change;
	}

	public void setLast_change(String last_change) {
		this.last_change = last_change;
	}

	public String getCreated_by() {
		return created_by;
	}

	public void setCreated_by(String created_by) {
		this.created_by = created_by;
	}

	public String getCreated_date() {
		return created_date;
	}

	public void setCreated_date(String created_date) {
		this.created_date = created_date;
	}

	public String getRequestorId() {
		return requestorId;
	}

	public void setRequestorId(String requestorId) {
		this.requestorId = requestorId;
	}

	public String getRequestorName() {
		return requestorName;
	}

	public void setRequestorName(String requestorName) {
		this.requestorName = requestorName;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getBase64() {
		return base64;
	}

	public void setBase64(String base64) {
		this.base64 = base64;
	}

	public String getData_type() {
		return data_type;
	}

	public void setData_type(String data_type) {
		this.data_type = data_type;
	}

	public String getResponsible() {
		return responsible;
	}

	public void setResponsible(String responsible) {
		this.responsible = responsible;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getNo_sk() {
		return no_sk;
	}

	public void setNo_sk(String no_sk) {
		this.no_sk = no_sk;
	}

	public String getEffective_date() {
		return effective_date;
	}

	public void setEffective_date(String effective_date) {
		this.effective_date = effective_date;
	}

	public String getDescriptionTemp() {
		return descriptionTemp;
	}

	public void setDescriptionTemp(String descriptionTemp) {
		this.descriptionTemp = descriptionTemp;
	}

	public Date getStartDateTemp() {
		return startDateTemp;
	}

	public void setStartDateTemp(Date startDateTemp) {
		this.startDateTemp = startDateTemp;
	}

	public Date getEndDateTemp() {
		return endDateTemp;
	}

	public void setEndDateTemp(Date endDateTemp) {
		this.endDateTemp = endDateTemp;
	}

	public String getDataTypeStr() {
		return dataTypeStr;
	}

	public void setDataTypeStr(String dataTypeStr) {
		this.dataTypeStr = dataTypeStr;
	}

}
