package com.mega.gateway.model;

import java.io.Serializable;

public class FamilyId implements Serializable {

	private static final long serialVersionUID = -9057610849520371931L;

	private String landscape;

	private String empId;

	private String start_date;

	private String end_date;
	
	private String family_type;

	private String seq;

}
