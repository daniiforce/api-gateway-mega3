//package com.mega.gateway.model;
//
//import java.io.Serializable;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.UUID;
//
//import javax.persistence.Column;
//import javax.persistence.ElementCollection;
//import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
//import javax.persistence.Id;
//import javax.persistence.Table;
//
//import org.hibernate.annotations.GenericGenerator;
//import org.hibernate.annotations.Type;
//
//import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
//
//@Entity
//@Table(name = "users")
//@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
//public class User implements Serializable {
//
//	private static final long serialVersionUID = 1L;
//
//	@Id
//	@Column(name = "user_id")
//	@GenericGenerator(name = "uuid-gen", strategy = "uuid2")
//	@GeneratedValue(generator = "uuid-gen")
//	@Type(type = "pg-uuid")
//	private UUID user_id;
//
//	@Column(name = "username")
//	private String username;
//
//	@Column(name = "password")
//	private String password;
//
////	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
////	@JoinColumn(name="role_id")
////	private List<Role> role;
//	
////	@OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
//	
////	@OneToOne(fetch = FetchType.LAZY)
////	@JoinColumn(name="role_id", nullable = false)
////	private Role role;
//	
//	private Boolean isActive;
//	
//	@ElementCollection
//	private List<String> tempRole = new ArrayList<String>();
//	
//	public User() {
//	}
//	
//	public User(String username, String password, List<String> tempRole, Boolean isActive) {
//		this.username = username;
//		this.password = password;
////		this.role = role;
//		this.tempRole = tempRole;
//		this.isActive = isActive;
//	}
//	
//	public List<String> getTempRole() {
//		return tempRole;
//	}
//
//	public void setTempRole(List<String> tempRole) {
//		this.tempRole = tempRole;
//	}
//
//	public UUID getUser_id() {
//		return user_id;
//	}
//
//	public void setUser_id(UUID user_id) {
//		this.user_id = user_id;
//	}
//
//	public String getUsername() {
//		return username;
//	}
//
//	public void setUsername(String username) {
//		this.username = username;
//	}
//
//	public String getPassword() {
//		return password;
//	}
//
//	public void setPassword(String password) {
//		this.password = password;
//	}
//
////	public Role getRole() {
////		return role;
////	}
////
////	public void setRole(Role role) {
////		this.role = role;
////	}
//
//	public Boolean getIsActive() {
//		return isActive;
//	}
//
////	public List<Role> getRole() {
////		return role;
////	}
////
////	public void setRole(List<Role> role) {
////		this.role = role;
////	}
//	
//	public void setIsActive(Boolean isActive) {
//		this.isActive = isActive;
//	}
//	
//	// private String firstName;
//	//
//	// private String lastName;
//	//
//	// private String phone;
//	//
//
//}
