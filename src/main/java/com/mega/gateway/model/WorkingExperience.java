package com.mega.gateway.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "hr_md_emp_md0009")
@IdClass(WorkingExperienceId.class)
public class WorkingExperience implements Serializable {

	private static final long serialVersionUID = -2097622423273371741L;

	@Id
	@Column(length = 3)
	private String landscape;

	@Id
	@Column(name = "emp_id", length = 8)
	private String empId;

	@Id
	@Column(name = "start_date", length = 8)
	private String startDate;

	@Id
	@Column(name = "end_date", length = 8)
	private String endDate;

	@Id
	@Column(length = 4)
	private String seq;

	@Column(length = 50)
	private String company;

	@Column(length = 50)
	private String job;

	@Column(length = 50)
	private String position;

	@Column(length = 50)
	private String location;

	@Column(length = 100)
	private String description;

	@Column(length = 100)
	private String reason_leaving;

	@Column(length = 2)
	private String industry;

	@Column(length = 20)
	private String phone_number;

	@Column(length = 100)
	private String supervisor_name;

	@Column(length = 100)
	private String supervisor_position;

	@Column(length = 100)
	private String working_exp;

	@Column(length = 3)
	private String status_reference;

	@Column(length = 3)
	private String doc_completion;

	@Column(length = 10)
	private String kompensasi_terakhir;

	@Column(length = 8)
	private String ikatan_dinas;

	@Column(length = 10)
	private String user_change;

	@Column(length = 14)
	private String last_change;

	@Column(length = 8)
	private String created_by;

	@Column(length = 14)
	private String created_date;

	// Temporary

	@Transient
	private String requestorId;

	@Transient
	private String requestorName;

	@Transient
	private String subject;

	@Transient
	private String descriptionTemp;

	@Transient
	private String employeeId;

	@Transient
	private String employeeName;

	@Transient
	private String comment;

	@Transient
	private String base64;

	@Transient
	private Date startDateTemp;

	@Transient
	private Date endDateTemp;

	@Transient
	private String industryStr;

	public String getLandscape() {
		return landscape;
	}

	public void setLandscape(String landscape) {
		this.landscape = landscape;
	}

	public String getEmpId() {
		return empId;
	}

	public void setEmpId(String empId) {
		this.empId = empId;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getIndustryStr() {
		return industryStr;
	}

	public void setIndustryStr(String industryStr) {
		this.industryStr = industryStr;
	}

	public String getSeq() {
		return seq;
	}

	public void setSeq(String seq) {
		this.seq = seq;
	}

	public String getUser_change() {
		return user_change;
	}

	public void setUser_change(String user_change) {
		this.user_change = user_change;
	}

	public String getLast_change() {
		return last_change;
	}

	public void setLast_change(String last_change) {
		this.last_change = last_change;
	}

	public String getCreated_by() {
		return created_by;
	}

	public void setCreated_by(String created_by) {
		this.created_by = created_by;
	}

	public String getCreated_date() {
		return created_date;
	}

	public void setCreated_date(String created_date) {
		this.created_date = created_date;
	}

	public String getRequestorId() {
		return requestorId;
	}

	public void setRequestorId(String requestorId) {
		this.requestorId = requestorId;
	}

	public String getRequestorName() {
		return requestorName;
	}

	public void setRequestorName(String requestorName) {
		this.requestorName = requestorName;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getBase64() {
		return base64;
	}

	public void setBase64(String base64) {
		this.base64 = base64;
	}

	public String getDescriptionTemp() {
		return descriptionTemp;
	}

	public void setDescriptionTemp(String descriptionTemp) {
		this.descriptionTemp = descriptionTemp;
	}

	public Date getStartDateTemp() {
		return startDateTemp;
	}

	public void setStartDateTemp(Date startDateTemp) {
		this.startDateTemp = startDateTemp;
	}

	public Date getEndDateTemp() {
		return endDateTemp;
	}

	public void setEndDateTemp(Date endDateTemp) {
		this.endDateTemp = endDateTemp;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getJob() {
		return job;
	}

	public void setJob(String job) {
		this.job = job;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getReason_leaving() {
		return reason_leaving;
	}

	public void setReason_leaving(String reason_leaving) {
		this.reason_leaving = reason_leaving;
	}

	public String getIndustry() {
		return industry;
	}

	public void setIndustry(String industry) {
		this.industry = industry;
	}

	public String getPhone_number() {
		return phone_number;
	}

	public void setPhone_number(String phone_number) {
		this.phone_number = phone_number;
	}

	public String getSupervisor_name() {
		return supervisor_name;
	}

	public void setSupervisor_name(String supervisor_name) {
		this.supervisor_name = supervisor_name;
	}

	public String getSupervisor_position() {
		return supervisor_position;
	}

	public void setSupervisor_position(String supervisor_position) {
		this.supervisor_position = supervisor_position;
	}

	public String getWorking_exp() {
		return working_exp;
	}

	public void setWorking_exp(String working_exp) {
		this.working_exp = working_exp;
	}

	public String getStatus_reference() {
		return status_reference;
	}

	public void setStatus_reference(String status_reference) {
		this.status_reference = status_reference;
	}

	public String getDoc_completion() {
		return doc_completion;
	}

	public void setDoc_completion(String doc_completion) {
		this.doc_completion = doc_completion;
	}

	public String getKompensasi_terakhir() {
		return kompensasi_terakhir;
	}

	public void setKompensasi_terakhir(String kompensasi_terakhir) {
		this.kompensasi_terakhir = kompensasi_terakhir;
	}

	public String getIkatan_dinas() {
		return ikatan_dinas;
	}

	public void setIkatan_dinas(String ikatan_dinas) {
		this.ikatan_dinas = ikatan_dinas;
	}

}
