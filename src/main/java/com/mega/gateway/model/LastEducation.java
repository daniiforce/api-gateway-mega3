package com.mega.gateway.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name = "base_cust_ref_edu_level")
@IdClass(LastEducationId.class)
public class LastEducation implements Serializable {

	private static final long serialVersionUID = -4358852912785884243L;

	@Id
	@Column(length = 3)
	private String landscape;

	@Id
	@Column(length = 2)
	private String code;

	@Column(length = 50)
	private String description;

	public String getLandscape() {
		return landscape;
	}

	public void setLandscape(String landscape) {
		this.landscape = landscape;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
