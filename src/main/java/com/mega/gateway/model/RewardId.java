package com.mega.gateway.model;

import java.io.Serializable;

public class RewardId implements Serializable {

	private static final long serialVersionUID = -2473293057365289710L;

	private String landscape;

	private String empId;

	private String startDate;

	private String endDate;

	private String seq;

}
