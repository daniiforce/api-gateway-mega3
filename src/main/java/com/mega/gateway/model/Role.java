//package com.mega.gateway.model;
//
//import java.io.Serializable;
//import java.util.UUID;
//
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.FetchType;
//import javax.persistence.GeneratedValue;
//import javax.persistence.Id;
//import javax.persistence.JoinColumn;
//import javax.persistence.ManyToOne;
//import javax.persistence.Table;
//
//import org.hibernate.annotations.GenericGenerator;
//import org.hibernate.annotations.Type;
//
//import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
//
//@Entity
//@Table(name="role")
//@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
//public class Role implements Serializable{
//	
//	private static final long serialVersionUID = 1L;
//
//	@Id
//    @Column(name = "role_id")
//    @GenericGenerator(name = "uuid-gen", strategy = "uuid2")
//    @GeneratedValue(generator = "uuid-gen")
//    @Type(type = "pg-uuid")
//    private UUID role_id;
//	
//	@Column(name = "label")
//	private String label;
//	
////	@ManyToOne(fetch = FetchType.LAZY)
////	@JoinColumn(name = "user_id")
////	private User user;
//	
//	public Role() {
//	}
//	
//	public Role(String label) {
//		this.label = label;
//	}
//
//	public UUID getRole_id() {
//		return role_id;
//	}
//
//	public void setRole_id(UUID role_id) {
//		this.role_id = role_id;
//	}
//
//	public String getLabel() {
//		return label;
//	}
//
//	public void setLabel(String label) {
//		this.label = label;
//	}
//	
////	public User getUser() {
////		return user;
////	}
////
////	public void setUser(User user) {
////		this.user = user;
////	}
//	
////    private String description;
////
////    private Boolean isActive;
//
//}