//package com.mega.gateway.model;
//
//import java.io.Serializable;
//import java.util.UUID;
//
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
//import javax.persistence.Id;
//import javax.persistence.Table;
//
//import org.hibernate.annotations.GenericGenerator;
//import org.hibernate.annotations.Type;
//
//@Entity
//@Table(name = "upload")
//public class Upload implements Serializable {
//
//	private static final long serialVersionUID = 1L;
//
//	@Id
//	@Column(name = "user_id")
//	@GenericGenerator(name = "uuid-gen", strategy = "uuid2")
//	@GeneratedValue(generator = "uuid-gen")
//	@Type(type = "pg-uuid")
//	private UUID user_id;
//
//	private String name;
//
//	@Column(columnDefinition="text")
//	private String base64;
//
//	public UUID getUser_id() {
//		return user_id;
//	}
//
//	public void setUser_id(UUID user_id) {
//		this.user_id = user_id;
//	}
//
//	public String getName() {
//		return name;
//	}
//
//	public void setName(String name) {
//		this.name = name;
//	}
//
//	public String getBase64() {
//		return base64;
//	}
//
//	public void setBase64(String base64) {
//		this.base64 = base64;
//	}
//
//}
