package com.mega.gateway.model;

import java.io.Serializable;

public class AddressId implements Serializable{
	
	private static final long serialVersionUID = -2732855192892478003L;

	private String landscape;

	private String empId;

	private String startDate;

	private String endDate;

	private String addressType;

	private String seq;

}
