package com.mega.gateway.model;

import java.util.Date;
import java.util.Map;

public class TasksDTO {
	// "task-id" : 6,
	// "task-name" : "Task",
	// "task-status" : "Reserved",
	// "task-actual-owner" : "ORISYS02",
	// "task-created-by" : "ORISYS02",
	// "task-proc-inst-id" : 56,
	// "task-proc-def-id" : "ess-kjar-1.JPAProcess",
	// "task-container-id" : "ess-kjar-1_0-SNAPSHOT",

	// "work-item-instance" : [ {
	// "work-item-id" : 56,
	// "work-item-name" : "Human Task",
	// "work-item-state" : 0,
	// "work-item-params" : {
	// "TaskName" : "Task",
	// "NodeName" : "Task",
	// "person" : {
	// "firstName" : "Stev",
	// "lasteName" : "Haw"
	// },
	// "Skippable" : "false",
	// "ActorId" : "ORISYS02"
	// },
	// "process-instance-id" : 56,
	// "container-id" : "ess-kjar-1_0-SNAPSHOT",
	// "node-instance-id" : 1,
	// "node-id" : 3
	// } ]

	// Tasks
	private String taskId;
	private String taskName;
	private String taskStatus;
	private String taskActualOwner;
	private String taskCreatedBy;
	private String taskProcInstId;
	private String taskProcDefId;
	private String taskContainerId;
	private String taskWorkItemId;
	private Date taskCreatedOn;

	// Workitems
	Map<String, Object> modelAtWorkItems;

	// Person
	private Person person;
	
	// Address
	private Address address;
	
	private String tempInfo;

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public String getTaskStatus() {
		return taskStatus;
	}

	public void setTaskStatus(String taskStatus) {
		this.taskStatus = taskStatus;
	}

	public String getTaskActualOwner() {
		return taskActualOwner;
	}

	public void setTaskActualOwner(String taskActualOwner) {
		this.taskActualOwner = taskActualOwner;
	}

	public String getTaskCreatedBy() {
		return taskCreatedBy;
	}

	public void setTaskCreatedBy(String taskCreatedBy) {
		this.taskCreatedBy = taskCreatedBy;
	}

	public String getTaskProcInstId() {
		return taskProcInstId;
	}

	public void setTaskProcInstId(String taskProcInstId) {
		this.taskProcInstId = taskProcInstId;
	}

	public String getTaskProcDefId() {
		return taskProcDefId;
	}

	public void setTaskProcDefId(String taskProcDefId) {
		this.taskProcDefId = taskProcDefId;
	}

	public String getTaskContainerId() {
		return taskContainerId;
	}

	public void setTaskContainerId(String taskContainerId) {
		this.taskContainerId = taskContainerId;
	}

	public Map<String, Object> getModelAtWorkItems() {
		return modelAtWorkItems;
	}

	public void setModelAtWorkItems(Map<String, Object> modelAtWorkItems) {
		this.modelAtWorkItems = modelAtWorkItems;
	}

	public String getTaskWorkItemId() {
		return taskWorkItemId;
	}

	public void setTaskWorkItemId(String taskWorkItemId) {
		this.taskWorkItemId = taskWorkItemId;
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public String getTempInfo() {
		return tempInfo;
	}

	public void setTempInfo(String tempInfo) {
		this.tempInfo = tempInfo;
	}

	public Date getTaskCreatedOn() {
		return taskCreatedOn;
	}

	public void setTaskCreatedOn(Date taskCreatedOn) {
		this.taskCreatedOn = taskCreatedOn;
	}
	
}
