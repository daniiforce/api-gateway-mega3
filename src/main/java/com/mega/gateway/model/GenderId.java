package com.mega.gateway.model;

import java.io.Serializable;

public class GenderId implements Serializable {

	private static final long serialVersionUID = 2720717255315037880L;

	private String landscape;

	private String code;

}
