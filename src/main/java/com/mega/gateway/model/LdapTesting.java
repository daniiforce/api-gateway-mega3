package com.mega.gateway.model;

public class LdapTesting {

	private String uid;
	private String loginDisabled;
	private String sn;
	private String passwordAllowChange;
	private String loginTime;
	private String passwordMinimumLength;
	private String passwordRequired;
	private String passwordUniqueRequired;
	private String objectClass;
	private String passwordExpirationInterval;
	private String givenName;
	private String co;
	private String cn;
	private String departmentNumber;
	private String ACL;
	private String displayName;
	private String passwordExpirationTime;

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getLoginDisabled() {
		return loginDisabled;
	}

	public void setLoginDisabled(String loginDisabled) {
		this.loginDisabled = loginDisabled;
	}

	public String getSn() {
		return sn;
	}

	public void setSn(String sn) {
		this.sn = sn;
	}

	public String getPasswordAllowChange() {
		return passwordAllowChange;
	}

	public void setPasswordAllowChange(String passwordAllowChange) {
		this.passwordAllowChange = passwordAllowChange;
	}

	public String getLoginTime() {
		return loginTime;
	}

	public void setLoginTime(String loginTime) {
		this.loginTime = loginTime;
	}

	public String getPasswordMinimumLength() {
		return passwordMinimumLength;
	}

	public void setPasswordMinimumLength(String passwordMinimumLength) {
		this.passwordMinimumLength = passwordMinimumLength;
	}

	public String getPasswordRequired() {
		return passwordRequired;
	}

	public void setPasswordRequired(String passwordRequired) {
		this.passwordRequired = passwordRequired;
	}

	public String getPasswordUniqueRequired() {
		return passwordUniqueRequired;
	}

	public void setPasswordUniqueRequired(String passwordUniqueRequired) {
		this.passwordUniqueRequired = passwordUniqueRequired;
	}

	public String getObjectClass() {
		return objectClass;
	}

	public void setObjectClass(String objectClass) {
		this.objectClass = objectClass;
	}

	public String getPasswordExpirationInterval() {
		return passwordExpirationInterval;
	}

	public void setPasswordExpirationInterval(String passwordExpirationInterval) {
		this.passwordExpirationInterval = passwordExpirationInterval;
	}

	public String getGivenName() {
		return givenName;
	}

	public void setGivenName(String givenName) {
		this.givenName = givenName;
	}

	public String getCo() {
		return co;
	}

	public void setCo(String co) {
		this.co = co;
	}

	public String getCn() {
		return cn;
	}

	public void setCn(String cn) {
		this.cn = cn;
	}

	public String getDepartmentNumber() {
		return departmentNumber;
	}

	public void setDepartmentNumber(String departmentNumber) {
		this.departmentNumber = departmentNumber;
	}

	public String getACL() {
		return ACL;
	}

	public void setACL(String aCL) {
		ACL = aCL;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getPasswordExpirationTime() {
		return passwordExpirationTime;
	}

	public void setPasswordExpirationTime(String passwordExpirationTime) {
		this.passwordExpirationTime = passwordExpirationTime;
	}

}
