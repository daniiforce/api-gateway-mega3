package com.mega.gateway.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "hr_md_emp_md0007")
@IdClass(FamilyId.class)
public class Family implements Serializable {

	private static final long serialVersionUID = 6882365484757396874L;

	@Id
	@Column(length = 3)
	private String landscape;

	@Id
	@Column(name = "emp_id", length = 8)
	private String empId;

	@Id
	@Column(length = 8)
	private String start_date;

	@Id
	@Column(length = 8)
	private String end_date;

	@Id
	@Column(length = 4)
	private String seq;

	@Id
	@Column(length = 2)
	private String family_type;

	@Column(length = 50)
	private String name;

	@Column(length = 2)
	private String gender;

	@Column(length = 50)
	private String birth_place;

	@Column(length = 8)
	private String birth_date;

	@Column(length = 50)
	private String date_of_death;

	@Column(length = 2)
	private String last_education;

	@Column(length = 200)
	private String address;

	@Column(length = 50)
	private String telp_home;

	@Column(length = 50)
	private String telp_office;

	@Column(length = 50)
	private String occupation;

	@Column(length = 2)
	private String medical_flag;

	@Column(length = 50)
	private String no_surat;

	@Column(length = 8)
	private String family_medical_code;

	@Column(length = 50)
	private String no_asuransi;

	@Column(length = 50)
	private String no_bpjs;

	@Column(length = 50)
	private String no_cug;

	@Column(length = 50)
	private String user_change;

	@Column(length = 14)
	private String last_change;

	@Column(length = 8)
	private String created_by;

	@Column(length = 14)
	private String created_date;

	// Temporary

	@Transient
	private String requestorId;

	@Transient
	private String requestorName;

	@Transient
	private String subject;

	@Transient
	private String descriptionTemp;

	@Transient
	private String employeeId;

	@Transient
	private String employeeName;

	@Transient
	private String comment;

	@Transient
	private String base64;

	@Transient
	private Date startDateTemp;

	@Transient
	private Date endDateTemp;

	@Transient
	private Date birthDateTemp;

	@Transient
	private Date dateOfDeathTemp;

	public String getLandscape() {
		return landscape;
	}

	public void setLandscape(String landscape) {
		this.landscape = landscape;
	}

	public String getEmpId() {
		return empId;
	}

	public void setEmpId(String empId) {
		this.empId = empId;
	}

	public String getStart_date() {
		return start_date;
	}

	public void setStart_date(String start_date) {
		this.start_date = start_date;
	}

	public String getEnd_date() {
		return end_date;
	}

	public void setEnd_date(String end_date) {
		this.end_date = end_date;
	}

	public String getSeq() {
		return seq;
	}

	public void setSeq(String seq) {
		this.seq = seq;
	}

	public String getUser_change() {
		return user_change;
	}

	public void setUser_change(String user_change) {
		this.user_change = user_change;
	}

	public String getLast_change() {
		return last_change;
	}

	public void setLast_change(String last_change) {
		this.last_change = last_change;
	}

	public String getCreated_by() {
		return created_by;
	}

	public void setCreated_by(String created_by) {
		this.created_by = created_by;
	}

	public String getCreated_date() {
		return created_date;
	}

	public void setCreated_date(String created_date) {
		this.created_date = created_date;
	}

	public String getRequestorId() {
		return requestorId;
	}

	public void setRequestorId(String requestorId) {
		this.requestorId = requestorId;
	}

	public String getRequestorName() {
		return requestorName;
	}

	public void setRequestorName(String requestorName) {
		this.requestorName = requestorName;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getBase64() {
		return base64;
	}

	public void setBase64(String base64) {
		this.base64 = base64;
	}

	public String getDescriptionTemp() {
		return descriptionTemp;
	}

	public void setDescriptionTemp(String descriptionTemp) {
		this.descriptionTemp = descriptionTemp;
	}

	public Date getStartDateTemp() {
		return startDateTemp;
	}

	public void setStartDateTemp(Date startDateTemp) {
		this.startDateTemp = startDateTemp;
	}

	public Date getEndDateTemp() {
		return endDateTemp;
	}

	public void setEndDateTemp(Date endDateTemp) {
		this.endDateTemp = endDateTemp;
	}

	public String getFamily_type() {
		return family_type;
	}

	public void setFamily_type(String family_type) {
		this.family_type = family_type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getBirth_place() {
		return birth_place;
	}

	public void setBirth_place(String birth_place) {
		this.birth_place = birth_place;
	}

	public String getBirth_date() {
		return birth_date;
	}

	public void setBirth_date(String birth_date) {
		this.birth_date = birth_date;
	}

	public String getDate_of_death() {
		return date_of_death;
	}

	public void setDate_of_death(String date_of_death) {
		this.date_of_death = date_of_death;
	}

	public String getLast_education() {
		return last_education;
	}

	public void setLast_education(String last_education) {
		this.last_education = last_education;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getTelp_home() {
		return telp_home;
	}

	public void setTelp_home(String telp_home) {
		this.telp_home = telp_home;
	}

	public String getTelp_office() {
		return telp_office;
	}

	public void setTelp_office(String telp_office) {
		this.telp_office = telp_office;
	}

	public String getOccupation() {
		return occupation;
	}

	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}

	public String getMedical_flag() {
		return medical_flag;
	}

	public void setMedical_flag(String medical_flag) {
		this.medical_flag = medical_flag;
	}

	public String getNo_surat() {
		return no_surat;
	}

	public void setNo_surat(String no_surat) {
		this.no_surat = no_surat;
	}

	public String getFamily_medical_code() {
		return family_medical_code;
	}

	public void setFamily_medical_code(String family_medical_code) {
		this.family_medical_code = family_medical_code;
	}

	public String getNo_asuransi() {
		return no_asuransi;
	}

	public void setNo_asuransi(String no_asuransi) {
		this.no_asuransi = no_asuransi;
	}

	public String getNo_bpjs() {
		return no_bpjs;
	}

	public void setNo_bpjs(String no_bpjs) {
		this.no_bpjs = no_bpjs;
	}

	public String getNo_cug() {
		return no_cug;
	}

	public void setNo_cug(String no_cug) {
		this.no_cug = no_cug;
	}

	public Date getBirthDateTemp() {
		return birthDateTemp;
	}

	public void setBirthDateTemp(Date birthDateTemp) {
		this.birthDateTemp = birthDateTemp;
	}

	public Date getDateOfDeathTemp() {
		return dateOfDeathTemp;
	}

	public void setDateOfDeathTemp(Date dateOfDeathTemp) {
		this.dateOfDeathTemp = dateOfDeathTemp;
	}

}
