package com.mega.gateway.model;

import java.io.Serializable;

public class WorkingExperienceId implements Serializable {

	private static final long serialVersionUID = 5840155244898793175L;

	private String landscape;

	private String empId;

	private String startDate;

	private String endDate;

	private String seq;

}
