package com.mega.gateway.security;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import com.mega.gateway.service.LdapService;

@Component
public class CustomAuthenticationProvider implements AuthenticationProvider {
	
	@Autowired
	private LdapService ldapService;
	
    @Override
    public Authentication authenticate(Authentication auth) throws AuthenticationException {
        String username = auth.getName();
        String password = auth.getCredentials().toString();
        
        String encodePassword = "";
        try {
        	encodePassword = encrypt("gek123", password, "iknowwhatyoudidlastnigth");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
        
//    	Map<String, String> verifyUserPassword = ldapService.verifyUserPassword("verifyPassword", username, encodePassword); 
//		String tempResponseCode = verifyUserPassword.get("ResponseCode");
		
//		if(tempResponseCode.equalsIgnoreCase("00") || tempResponseCode.equalsIgnoreCase("01")) {
			SimpleGrantedAuthority authority = new SimpleGrantedAuthority("ADMIN");
			List<SimpleGrantedAuthority> updatedAuthorities = new ArrayList<SimpleGrantedAuthority>();
			updatedAuthorities.add(authority);
			
			return new UsernamePasswordAuthenticationToken(username, password, updatedAuthorities);
//		}else {
//			throw new BadCredentialsException("User " + username + " was not registered on LDAP");
//		}
    }
 
    @Override
    public boolean supports(Class<?> auth) {
        return auth.equals(UsernamePasswordAuthenticationToken.class);
    }
    
	public static String encrypt(String salt,String password,String secret) throws NoSuchAlgorithmException {
		
		if(salt.length() > 2) {
	        salt = salt.substring(0, 2);	
		}
		String md5_text = salt + secret;

		MessageDigest md = MessageDigest.getInstance("MD5");
		md.update(md5_text.getBytes());
		byte[] digest = md.digest();
		byte[] bsalt = salt.getBytes();
		byte[] pwd = password.getBytes();
		

		int length  = salt.length() + (digest.length * (pwd.length + 16) / 16);	
		
		int i = 0,p = 0,j = 0;
		byte[] result = new byte[99999];
		
		for(;i<bsalt.length;i++) {
			result[i] = bsalt[i];
		}
		for(;i<length;i++) {
			if(p < pwd.length) {
				result[i] = (byte) (pwd[p] ^ digest[(j%digest.length)]);
			}else {
				result[i] = (byte) (0^digest[(j%digest.length)]);
			}
			j++;
			p++;
		}
		String encPassword = Base64.getEncoder().withoutPadding().encodeToString(trim(result));
		return encPassword;
	}
	
	static byte[] trim(byte[] bytes)
	{
	    int i = bytes.length - 1;
	    while (i >= 0 && bytes[i] == 0)
	    {
	        --i;
	    }

	    return Arrays.copyOf(bytes, i + 1);
	}

}
