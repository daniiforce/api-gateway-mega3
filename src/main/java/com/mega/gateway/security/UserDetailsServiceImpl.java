/*
package com.mega.gateway.security;


import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.mega.gateway.config.UserNotActivatedException;
import com.mega.gateway.model.User;
import com.mega.gateway.repository.UserRepository;
import com.mega.gateway.service.LdapService;

@Component("userDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService {

    private final Logger log = LoggerFactory.getLogger(UserDetailsServiceImpl.class);

    @Autowired
    private UserRepository userRepository;
    
    @Autowired
	private LdapService ldapService;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(final String login)
    {
        log.debug("Authenticating {}", login);
        String lowercaseLogin = login.toLowerCase();
        Optional<User> userFromDatabase = userRepository.findByUsernameIgnoreCase(lowercaseLogin);

        return userFromDatabase.map(user -> {
        	
//        	Map<String, String> verifyUserPassword = ldapService.verifyUserPassword("verifyPassword", user.getUsername(), user.getPassword()); 
//    		String tempResponseCode = verifyUserPassword.get("ResponseCode");
//    		
//    		if(!tempResponseCode.equalsIgnoreCase("00")) {
//    			throw new UserNotActivatedException("User " + lowercaseLogin + " was not registered on LDAP");
//    		}
        	
            if (user.getIsActive() == null || !user.getIsActive()) {
                throw new UserNotActivatedException("User " + lowercaseLogin + " was not activated");
            }

            List<GrantedAuthority> grantedAuthorities = user.getTempRole().stream()
                    .map(role -> new SimpleGrantedAuthority(role))
                    .collect(Collectors.toList());

            return new org.springframework.security.core.userdetails.User(lowercaseLogin,
                    user.getPassword(), grantedAuthorities);
        })
                .orElseThrow(() -> new UsernameNotFoundException("User " + lowercaseLogin + " was not found in the " +
                "database"));
    }
}

*/