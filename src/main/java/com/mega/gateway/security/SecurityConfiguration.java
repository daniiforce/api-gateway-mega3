package com.mega.gateway.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.data.repository.query.SecurityEvaluationContextExtension;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import com.mega.gateway.config.Http401UnauthorizedEntryPoint;
import com.mega.gateway.config.LdapEncryption;
import com.mega.gateway.security.jwt.JwtConfigurer;
import com.mega.gateway.security.jwt.TokenProvider;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    private Http401UnauthorizedEntryPoint authenticationEntryPoint;

    //user get from database
//    @Autowired
//    private UserDetailsService userDetailsService;

    @Autowired
    private TokenProvider tokenProvider;
    
    @Autowired
    CustomAuthenticationProvider customAuthProvider;

    // For encoding using password encoder
//    @Bean
//    public PasswordEncoder passwordEncoder() {
//    	PasswordEncoder encoder = new LdapEncryption();
//        return encoder;
//    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
    	//for using user from database
//        		auth
//                .userDetailsService(userDetailsService)
//                .passwordEncoder(passwordEncoder());

    	auth
        .authenticationProvider(customAuthProvider);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .exceptionHandling()
                .authenticationEntryPoint(authenticationEntryPoint)
                .and()
                .csrf().disable()
                .headers().frameOptions().disable()
                .and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authorizeRequests()
                .antMatchers("/bower_components/**", "/dist/**", "/plugins/**","/reportTemplate/**").permitAll()
                .antMatchers("/auth/**").permitAll()
                .antMatchers("/ldap/**").permitAll()
                .antMatchers("/user/**").permitAll()
                .antMatchers("/task/**").permitAll()
                .antMatchers("/address/**").permitAll()
                .antMatchers("/process/**").permitAll()
                .antMatchers("/tasks/**").permitAll()
                .antMatchers("/person/**").permitAll()
                .antMatchers("/upload/**").permitAll()
                .antMatchers("/address_type/**").permitAll()
                .antMatchers("/country/**").permitAll()
                .antMatchers("/province/**").permitAll()
                .antMatchers("/reward/**").permitAll()
                .antMatchers("/data_type/**").permitAll()
                .antMatchers("/working_experience/**").permitAll()
                .antMatchers("/industry/**").permitAll()
                .antMatchers("/family/**").permitAll()
                .antMatchers("/last_education/**").permitAll()
                .antMatchers("/gender/**").permitAll()
                .antMatchers("/family_type/**").permitAll()
                .anyRequest().authenticated()
                .and()
                .formLogin()
                    .failureUrl("/login?error")
                    .loginPage("/login")
                    .defaultSuccessUrl("/")
                    .usernameParameter("username")
                    .passwordParameter("password")
                    .permitAll()
                .and()
                    .rememberMe()
                    .tokenValiditySeconds(1 * 24 * 60 * 60)
                .and()
                	.logout()
                	.logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
                	.logoutSuccessUrl("/login")
                	.permitAll()
                .and()
                .apply(securityConfigurerAdapter());
    }

    private JwtConfigurer securityConfigurerAdapter() {
        return new JwtConfigurer(tokenProvider);
    }

    @Bean
    public SecurityEvaluationContextExtension securityEvaluationContextExtension() {
        return new SecurityEvaluationContextExtension();
    }
}
