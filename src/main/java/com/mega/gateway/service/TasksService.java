package com.mega.gateway.service;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.mega.gateway.model.Address;
import com.mega.gateway.model.Family;
import com.mega.gateway.model.Reward;
import com.mega.gateway.model.WorkingExperience;
import com.mega.gateway.utils.HeaderDataSetup;

@Service
public class TasksService {

	private final RestTemplate restTemplate;

	@Autowired
	public TasksService(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}
	
	@Value("${jbpm.host.and.port}")
	private String JBPM_HOST_PORT;

	@Value("${jbpm.rest.server}")
	private String JBPM_REST_SERVER;

	@Value("${jbpm.tasks.owners}")
	private String JBPM_TASKS_OWNERS;
	
	@Value("${jbpm.get.task}")
	private String JBPM_GET_TASK;

	@Value("${jbpm.containers}")
	private String JBPM_CONTAINERS;
	
	@Value("${jbpm.get.task}")
	private String JBPM_TASKS;

	@Value("${jbpm.states.started}")
	private String JBPM_STATES_STARTED;

	@Value("${jbpm.states.completed}")
	private String JBPM_STATES_COMPLETED;

	public Map<String, Object> ownerTasks(String username, String password) {
		HeaderDataSetup headerDataSetup = new HeaderDataSetup(username, password);
		HttpHeaders headers = headerDataSetup.getHttpHeaders();

		HttpEntity httpEntity = new HttpEntity<>(headers);

		ResponseEntity<Map> responseTasks = restTemplate.exchange(JBPM_HOST_PORT + JBPM_REST_SERVER + JBPM_TASKS_OWNERS,
				HttpMethod.GET, httpEntity, Map.class);

		Map<String, Object> responseTask = responseTasks.getBody();
		return responseTask;
	}
	
	public Map<String, Object> getTask(String username, String password, String taskId) {
		HeaderDataSetup headerDataSetup = new HeaderDataSetup(username, password);
		HttpHeaders headers = headerDataSetup.getHttpHeaders();

		HttpEntity httpEntity = new HttpEntity<>(headers);

		ResponseEntity<Map> responseTasks = restTemplate.exchange(JBPM_HOST_PORT + JBPM_REST_SERVER + JBPM_CONTAINERS + JBPM_GET_TASK + taskId,
				HttpMethod.GET, httpEntity, Map.class);

		Map<String, Object> responseTask = responseTasks.getBody();
		return responseTask;
	}
	
	public int startCompleteTask(String username, String password, String taskId, LinkedHashMap<String, Object> data) {
		
		HeaderDataSetup headerDataSetup = new HeaderDataSetup(username, password);
		HttpHeaders headers = headerDataSetup.getHttpHeaders();

		Map<String, Object> mapTemp = new HashMap<>();

		if(data.get("address") != null) {
			Map<String, Object> mapAddress = (Map<String, Object>) data.get("address");
			Address address = new Address();
			
			address.setAddressType(mapAddress.get("addressType") != null ? mapAddress.get("addressType").toString() : "");
			address.setCellphone_num(mapAddress.get("cellphone_num") !=  null ? mapAddress.get("cellphone_num").toString() : "");
			address.setCity(mapAddress.get("city") != null ? mapAddress.get("city").toString() : "");
			address.setContact_person(mapAddress.get("contact_person") != null ? mapAddress.get("contact_person").toString() : "");
			address.setCountry(mapAddress.get("country") != null ? mapAddress.get("country").toString() : "");
			address.setCreated_by(mapAddress.get("created_by") != null ? mapAddress.get("created_by").toString() : "");
			address.setCreated_date(mapAddress.get("created_date") != null ? mapAddress.get("created_date").toString() : "");
			address.setEmpId(mapAddress.get("empId") != null ? mapAddress.get("empId").toString() : "");
			address.setEndDate(mapAddress.get("endDate") != null ? mapAddress.get("endDate").toString() : "");
			address.setKecamatan(mapAddress.get("kecamatan") != null ? mapAddress.get("kecamatan").toString() : "" );
			address.setKelurahan(mapAddress.get("kelurahan") != null ? mapAddress.get("kelurahan").toString() : "");
			address.setKepemilikan(mapAddress.get("kepemilikan") != null ? mapAddress.get("kepemilikan").toString() : "");
			address.setLandscape(mapAddress.get("landscape") != null ? mapAddress.get("landscape").toString() : "");
			address.setLast_change(mapAddress.get("last_change") != null ? mapAddress.get("last_change").toString() : "");
			address.setLocation(mapAddress.get("location") != null ? mapAddress.get("location").toString() : "");
			address.setPostalcode(mapAddress.get("postalcode") != null ? mapAddress.get("postalcode").toString() : "");
			address.setProvince(mapAddress.get("province") != null ? mapAddress.get("province").toString() : "");
			address.setRt(mapAddress.get("rt") != null ? mapAddress.get("rt").toString() : "");
			address.setRw(mapAddress.get("rw") != null ? mapAddress.get("rw").toString() : "");
			address.setSeq(mapAddress.get("seq") != null ? mapAddress.get("seq").toString() : "");
			address.setStartDate(mapAddress.get("startDate") != null ? mapAddress.get("startDate").toString() : "");
			address.setStreet(mapAddress.get("street") != null ? mapAddress.get("street").toString() : "");
			address.setTelp_num(mapAddress.get("telp_num") != null ? mapAddress.get("telp_num").toString() : "");
			address.setUser_change(mapAddress.get("user_change") != null ? mapAddress.get("user_change").toString() : "");
			address.setSubject(mapAddress.get("subject") != null ? mapAddress.get("subject").toString() : "");
			address.setDescription(mapAddress.get("description") != null ? mapAddress.get("description").toString() : "");
			address.setComment(mapAddress.get("comment") != null ? mapAddress.get("comment").toString() : "");
			address.setBase64(mapAddress.get("base64") != null ? mapAddress.get("base64").toString() : "");
			address.setRequestorId(mapAddress.get("requestorId") != null ? mapAddress.get("requestorId").toString() : "");
			address.setRequestorName(mapAddress.get("requestorName") != null ? mapAddress.get("requestorName").toString() : "");
			address.setEmployeeId(mapAddress.get("employeeId") != null ? mapAddress.get("employeeId").toString() : "");
			address.setEmployeeName(mapAddress.get("employeeName") != null ? mapAddress.get("employeeName").toString() : "");

			mapTemp.put("address", address);
		}else if(data.get("reward") != null) {
			Map<String, Object> mapReward = (Map<String, Object>) data.get("reward");
			Reward reward = new Reward();
			
			reward.setAmount(mapReward.get("amount") != null ? mapReward.get("amount").toString() : "");
			reward.setBase64(mapReward.get("base64") != null ? mapReward.get("base64").toString() : "");
			reward.setComment(mapReward.get("comment") != null ? mapReward.get("comment").toString() : "");
			reward.setCreated_by(mapReward.get("created_by") != null ? mapReward.get("created_by").toString() : "");
			reward.setCreated_date(mapReward.get("created_date") != null ? mapReward.get("created_date").toString() : "");
			reward.setData_type(mapReward.get("data_type") != null ? mapReward.get("data_type").toString() : "");
			reward.setDescription(mapReward.get("description") != null ? mapReward.get("description").toString() : "");
			reward.setEffective_date(mapReward.get("effective_date") != null ? mapReward.get("effective_date").toString() : "");
			reward.setEmpId(mapReward.get("empId") != null ? mapReward.get("empId").toString() : "");
			reward.setEmployeeId(mapReward.get("employeeId") != null ? mapReward.get("employeeId").toString() : "");
			reward.setEmployeeName(mapReward.get("employeeName") != null ? mapReward.get("employeeName").toString() : "");
			reward.setEndDate(mapReward.get("endDate") != null ? mapReward.get("endDate").toString() : "");
			reward.setLandscape(mapReward.get("landscape") != null ? mapReward.get("landscape").toString() : "");
			reward.setLast_change(mapReward.get("last_change") != null ? mapReward.get("last_change").toString() : "");
			reward.setNo_sk(mapReward.get("no_sk") != null ? mapReward.get("no_sk").toString() : "");
			reward.setRequestorId(mapReward.get("requestorId") != null ? mapReward.get("requestorId").toString() : "");
			reward.setRequestorName(mapReward.get("requestorName") != null ? mapReward.get("requestorName").toString() : "");
			reward.setResponsible(mapReward.get("responsible") != null ? mapReward.get("responsible").toString() : "");
			reward.setSeq(mapReward.get("seq") != null ? mapReward.get("seq").toString() : "");
			reward.setStartDate(mapReward.get("startDate") != null ? mapReward.get("startDate").toString() : "");
			reward.setSubject(mapReward.get("subject") != null ? mapReward.get("subject").toString() : "");
			reward.setUser_change(mapReward.get("user_change") != null ? mapReward.get("user_change").toString() : "");
			reward.setDescriptionTemp(mapReward.get("descriptionTemp") != null ? mapReward.get("descriptionTemp").toString() : "");
			
			mapTemp.put("reward", reward);
		}else if(data.get("workExperience") != null) {
			Map<String, Object> mapWorkingExperience = (Map<String, Object>) data.get("workExperience");
			WorkingExperience workingExperience = new WorkingExperience();
			
			workingExperience.setBase64(mapWorkingExperience.get("base64") != null ? mapWorkingExperience.get("base64").toString() : "");
			workingExperience.setComment(mapWorkingExperience.get("comment") != null ? mapWorkingExperience.get("comment").toString() : "");
			workingExperience.setCompany(mapWorkingExperience.get("company") != null ? mapWorkingExperience.get("company").toString() : "");
			workingExperience.setCreated_by(mapWorkingExperience.get("created_by") != null ? mapWorkingExperience.get("created_by").toString() : "");
			workingExperience.setCreated_date(mapWorkingExperience.get("created_date") != null ? mapWorkingExperience.get("created_date").toString() : "");
			workingExperience.setDescription(mapWorkingExperience.get("description") != null ? mapWorkingExperience.get("description").toString() : "");
			workingExperience.setDescriptionTemp(mapWorkingExperience.get("descriptionTemp") != null ? mapWorkingExperience.get("descriptionTemp").toString() : "");
			workingExperience.setDoc_completion(mapWorkingExperience.get("doc_completion") != null ? mapWorkingExperience.get("doc_completion").toString() : "");
			workingExperience.setEmpId(mapWorkingExperience.get("empId") != null ? mapWorkingExperience.get("empId").toString() : "");
			workingExperience.setEmployeeId(mapWorkingExperience.get("employeeId") != null ? mapWorkingExperience.get("employeeId").toString() : "");
			workingExperience.setEmployeeName(mapWorkingExperience.get("employeeName") != null ? mapWorkingExperience.get("employeeName").toString() : "");
			workingExperience.setEndDate(mapWorkingExperience.get("endDate") != null ? mapWorkingExperience.get("endDate").toString() : "");
			workingExperience.setIkatan_dinas(mapWorkingExperience.get("ikatan_dinas") != null ? mapWorkingExperience.get("ikatan_dinas").toString() : "");
			workingExperience.setIndustry(mapWorkingExperience.get("industry") != null ? mapWorkingExperience.get("industry").toString() : "");
			workingExperience.setJob(mapWorkingExperience.get("job") != null ? mapWorkingExperience.get("job").toString() : "");
			workingExperience.setKompensasi_terakhir(mapWorkingExperience.get("kompensasi_terakhir") != null ? mapWorkingExperience.get("kompensasi_terakhir").toString() : "");
			workingExperience.setLandscape(mapWorkingExperience.get("landscape") != null ? mapWorkingExperience.get("landscape").toString() : "");
			workingExperience.setLast_change(mapWorkingExperience.get("last_change") != null ? mapWorkingExperience.get("last_change").toString() : "");
			workingExperience.setLocation(mapWorkingExperience.get("location") != null ? mapWorkingExperience.get("location").toString() : "");
			workingExperience.setPhone_number(mapWorkingExperience.get("phone_number") != null ? mapWorkingExperience.get("phone_number").toString() : "");
			workingExperience.setPosition(mapWorkingExperience.get("position") != null ? mapWorkingExperience.get("position").toString() : "");
			workingExperience.setReason_leaving(mapWorkingExperience.get("reason_leaving") != null ? mapWorkingExperience.get("reason_leaving").toString() : "");
			workingExperience.setRequestorId(mapWorkingExperience.get("requestorId") != null ? mapWorkingExperience.get("requestorId").toString() : "");
			workingExperience.setRequestorName(mapWorkingExperience.get("requestorName") != null ? mapWorkingExperience.get("requestorName").toString() : "");
			workingExperience.setSeq(mapWorkingExperience.get("seq") != null ? mapWorkingExperience.get("seq").toString() : "");
			workingExperience.setStartDate(mapWorkingExperience.get("startDate") != null ? mapWorkingExperience.get("startDate").toString() : "");
			workingExperience.setStatus_reference(mapWorkingExperience.get("status_reference") != null ? mapWorkingExperience.get("status_reference").toString() : "");
			workingExperience.setSubject(mapWorkingExperience.get("subject") != null ? mapWorkingExperience.get("subject").toString() : "");
			workingExperience.setSupervisor_name(mapWorkingExperience.get("supervisor_name") != null ? mapWorkingExperience.get("supervisor_name").toString() : "");
			workingExperience.setSupervisor_position(mapWorkingExperience.get("supervisor_position") != null ? mapWorkingExperience.get("supervisor_position").toString() : "");
			workingExperience.setUser_change(mapWorkingExperience.get("user_change") != null ? mapWorkingExperience.get("user_change").toString() : "");
			workingExperience.setWorking_exp(mapWorkingExperience.get("working_exp") != null ? mapWorkingExperience.get("working_exp").toString() : "");
			
			mapTemp.put("workExperience", workingExperience);
		}else if(data.get("family") != null) {
			Map<String, Object> mapFamily = (Map<String, Object>) data.get("family");
			Family family = new Family();
			
			family.setAddress(mapFamily.get("address") != null ? mapFamily.get("address").toString() : "");
			family.setBase64(mapFamily.get("base64") != null ? mapFamily.get("base64").toString() : "");
			family.setBirth_date(mapFamily.get("birth_date") != null ? mapFamily.get("birth_date").toString() : "");
			family.setBirth_place(mapFamily.get("birth_place") != null ? mapFamily.get("birth_place").toString() : "");
			family.setComment(mapFamily.get("comment") != null ? mapFamily.get("comment").toString() : "");
			family.setCreated_by(mapFamily.get("created_by") != null ? mapFamily.get("created_by").toString() : "");
			family.setCreated_date(mapFamily.get("created_date") != null ? mapFamily.get("created_date").toString() : "");
			family.setDate_of_death(mapFamily.get("date_of_death") != null ? mapFamily.get("date_of_death").toString() : "");
			family.setDescriptionTemp(mapFamily.get("descriptionTemp") != null ? mapFamily.get("descriptionTemp").toString() : "");
			family.setEmpId(mapFamily.get("empId") != null ? mapFamily.get("empId").toString() : "");
			family.setEmployeeId(mapFamily.get("employeeId") != null ? mapFamily.get("employeeId").toString() : "");
			family.setEmployeeName(mapFamily.get("employeeName") != null ? mapFamily.get("employeeName").toString() : "");
			family.setEnd_date(mapFamily.get("end_date") != null ? mapFamily.get("end_date").toString() : "");
			family.setFamily_medical_code(mapFamily.get("family_medical_code") != null ? mapFamily.get("family_medical_code").toString() : "");
			family.setFamily_type(mapFamily.get("family_type") != null ? mapFamily.get("family_type").toString() : "");
			family.setGender(mapFamily.get("gender") != null ? mapFamily.get("gender").toString() : "");
			family.setLandscape(mapFamily.get("landscape") != null ? mapFamily.get("landscape").toString() : "");
			family.setLast_change(mapFamily.get("last_change") != null ? mapFamily.get("last_change").toString() : "");
			family.setLast_education(mapFamily.get("last_education") != null ? mapFamily.get("last_education").toString() : "");
			family.setMedical_flag(mapFamily.get("medical_flag") != null ? mapFamily.get("medical_flag").toString() : "");
			family.setName(mapFamily.get("name") != null ? mapFamily.get("name").toString() : "");
			family.setNo_asuransi(mapFamily.get("no_asuransi") != null ? mapFamily.get("no_asuransi").toString() : "");
			family.setNo_bpjs(mapFamily.get("no_bpjs") != null ? mapFamily.get("no_bpjs").toString() : "");
			family.setNo_cug(mapFamily.get("no_cug") != null ? mapFamily.get("no_cug").toString() : "");
			family.setNo_surat(mapFamily.get("no_surat") != null ? mapFamily.get("no_surat").toString() : "");
			family.setOccupation(mapFamily.get("occupation") != null ? mapFamily.get("occupation").toString() : "");
			family.setRequestorId(mapFamily.get("requestorId") != null ? mapFamily.get("requestorId").toString() : "");
			family.setRequestorName(mapFamily.get("requestorName") != null ? mapFamily.get("requestorName").toString() : "");
			family.setSeq(mapFamily.get("seq") != null ? mapFamily.get("seq").toString() : "");
			family.setStart_date(mapFamily.get("start_date") != null ? mapFamily.get("start_date").toString() : "");
			family.setSubject(mapFamily.get("subject") != null ? mapFamily.get("subject").toString() : "");
			family.setTelp_home(mapFamily.get("telp_home") != null ? mapFamily.get("telp_home").toString() : "");
			family.setTelp_office(mapFamily.get("telp_office") != null ? mapFamily.get("telp_office").toString() : "");
			family.setUser_change(mapFamily.get("user_change") != null ? mapFamily.get("user_change").toString() : "");
			
			mapTemp.put("family", family);
		}
		
		String approval = data.get("approval").toString();
		mapTemp.put("approval", approval);
		
		HttpEntity httpEntity = new HttpEntity<>(mapTemp, headers);
		
		//start task
		ResponseEntity<HttpStatus> httpStatus = restTemplate.exchange(
				JBPM_HOST_PORT + JBPM_REST_SERVER + JBPM_CONTAINERS + JBPM_TASKS + taskId + JBPM_STATES_STARTED,
				HttpMethod.PUT, httpEntity, HttpStatus.class);
		
		//complete task
		if(httpStatus.getStatusCode() == HttpStatus.CREATED){
			httpStatus = restTemplate.exchange(
					JBPM_HOST_PORT + JBPM_REST_SERVER + JBPM_CONTAINERS + JBPM_TASKS + taskId + JBPM_STATES_COMPLETED,
					HttpMethod.PUT, httpEntity, HttpStatus.class);
		}
		return httpStatus.getStatusCodeValue();
	}

}
