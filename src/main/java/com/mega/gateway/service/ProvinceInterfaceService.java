package com.mega.gateway.service;

import java.util.List;

import org.springframework.stereotype.Component;

import com.mega.gateway.model.Province;

@Component
public interface ProvinceInterfaceService {
	
	List<Province> findAll();
	
	Province findByCode(String code);
}
