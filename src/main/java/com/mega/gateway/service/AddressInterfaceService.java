package com.mega.gateway.service;

import java.util.List;

import org.springframework.stereotype.Component;

import com.mega.gateway.model.Address;

@Component
public interface AddressInterfaceService {
	
	Address save(Address address);
	
	Address findByEmpId(String empId);
	
	List<Address> findByEmpIdEquals(String empId);
	
	List<Address> findAll();
	
	Address findByEmpIdAndStartDateAndEndDateAndAddressTypeAndSeq(String empId, String startDate, String endDate, String addressType, String seq);
}
