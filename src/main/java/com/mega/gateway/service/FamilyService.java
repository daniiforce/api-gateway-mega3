package com.mega.gateway.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.mega.gateway.model.Family;
import com.mega.gateway.utils.HeaderDataSetup;

@Service
public class FamilyService {

	private final RestTemplate restTemplate;

	@Autowired
	public FamilyService(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}
	
	@Value("${jbpm.host.and.port}")
	private String JBPM_HOST_PORT;
	
	@Value("${jbpm.rest.server}")
	private String JBPM_REST_SERVER;
	
	@Value("${jbpm.containers}")
	private String JBPM_CONTAINERS;

	@Value("${jbpm.processes}")
	private String JBPM_PROCESSES;
	
	@Value("${jbpm.instances}")
	private String JBPM_INSTANCES;
	
	@Value("${jbpm.get.task}")
	private String JBPM_TASKS;

	@Value("${jbpm.states.started}")
	private String JBPM_STATES_STARTED;

	@Value("${jbpm.states.completed}")
	private String JBPM_STATES_COMPLETED;

	public int submitFamily(String process_id, Family family, String username) {
		
		HeaderDataSetup headerDataSetup = new HeaderDataSetup(username, "B@nkMega1");
		HttpHeaders headers = headerDataSetup.getHttpHeaders();

		Map<String, Object> mapAddress = new HashMap<>();
		mapAddress.put("family", family);
		
		HttpEntity httpEntity = new HttpEntity<>(mapAddress, headers);

		ResponseEntity<Integer> i = restTemplate.exchange(
				JBPM_HOST_PORT + JBPM_REST_SERVER + JBPM_CONTAINERS + JBPM_PROCESSES + process_id + "/" + JBPM_INSTANCES,
				HttpMethod.POST, httpEntity, Integer.class);

		int processInstanceId = i.getBody();
		
		//get task id
		Map<String, Object> processInstance = restTemplate.getForObject(JBPM_HOST_PORT + JBPM_REST_SERVER + JBPM_CONTAINERS + JBPM_PROCESSES + JBPM_INSTANCES + "/" + processInstanceId, Map.class);
		Map<String, Object> activeUserTasks =  (Map<String, Object>) processInstance.get("active-user-tasks");
		List<Map<String, Object>> taskSummary = (List<Map<String, Object>>) activeUserTasks.get("task-summary");
		Map<String, Object> getTaskSummary = taskSummary.get(0);
		
		String taskId = getTaskSummary.get("task-id").toString();
		
		//start task
		ResponseEntity<HttpStatus> httpStatus = restTemplate.exchange(
				JBPM_HOST_PORT + JBPM_REST_SERVER + JBPM_CONTAINERS + JBPM_TASKS + taskId + JBPM_STATES_STARTED,
				HttpMethod.PUT, httpEntity, HttpStatus.class);
		
		//complete task
		if(httpStatus.getStatusCode() == HttpStatus.CREATED){
			httpStatus = restTemplate.exchange(
					JBPM_HOST_PORT + JBPM_REST_SERVER + JBPM_CONTAINERS + JBPM_TASKS + taskId + JBPM_STATES_COMPLETED,
					HttpMethod.PUT, httpEntity, HttpStatus.class);
		}
		return httpStatus.getStatusCodeValue();
	}

}
