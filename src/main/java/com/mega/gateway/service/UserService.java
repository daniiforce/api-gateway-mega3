//package com.mega.gateway.service;
//
//import java.util.Optional;
//
//import org.springframework.stereotype.Component;
//
//import com.mega.gateway.model.User;
//
//@Component
//public interface UserService {
//	
//	Optional<User> findByUsername(String username);
//	
//	Optional<User> findByUsernameOption(String username);
//
//}
