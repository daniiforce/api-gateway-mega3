package com.mega.gateway.service;

import java.util.List;

import org.springframework.stereotype.Component;

import com.mega.gateway.model.Gender;

@Component
public interface GenderInterfaceService {
	
	List<Gender> findAll();
}
