package com.mega.gateway.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class ProcessService {

	private final RestTemplate restTemplate;

	@Autowired
	public ProcessService(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}
	
	public Map<String, Object> listProcess() {
		Map<String, Object> listProcess = new HashMap<String,Object>();
		try {
			listProcess = restTemplate.getForObject("http://localhost:8090/rest/server/containers/ess-kjar/processes", Map.class);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return listProcess;
	}
	
	public Map<String, Object> listWorkItems(int instanceId) {
		Map<String, Object> listProcess = new HashMap<String,Object>();
		try {
			listProcess = restTemplate.getForObject("http://localhost:8090/rest/server/containers/ess-kjar/processes/instances/" + instanceId + "/workitems", Map.class);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return listProcess;
	}
	
}
