package com.mega.gateway.service;

import java.util.List;

import org.springframework.stereotype.Component;

import com.mega.gateway.model.LastEducation;;

@Component
public interface LastEducationInterfaceService {
	
	List<LastEducation> findAll();
}
