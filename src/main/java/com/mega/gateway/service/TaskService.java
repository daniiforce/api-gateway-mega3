package com.mega.gateway.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class TaskService {
	@Autowired
	private RestTemplate restTemplate;
	
	public Map<String, Object> listProcess() {
		Map<String, Object> listProcess = new HashMap<String,Object>();
		try {
			listProcess = restTemplate.getForObject("http://localhost:8090/rest/server/containers/ess-kjar/processes", Map.class);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return listProcess;
	}
	
	public int submitTask(String process_id){
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		Map<String, Object> mapPerson = new HashMap<>();
		mapPerson.put("firstName", "jbpm");
		mapPerson.put("lasteName", "17");
		
		HttpEntity httpEntity = new HttpEntity<>(mapPerson, headers);
		
		ResponseEntity<Integer> i = restTemplate.exchange("http://localhost:8090/rest/server/containers/ess-kjar/processes/"+ process_id + "/instances", HttpMethod.POST, httpEntity, Integer.class);
		int h = i.getBody();
		return h;
	}
}
