package com.mega.gateway.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.mega.gateway.model.LdapTesting;

@Service
public class LdapService {
	
	@Autowired
	private RestTemplate restTemplate;
	
	public LdapTesting testing(String operation, String id) {
		String url = "http://10.14.18.177:5623/ldapService?operation=" + operation +
				"&id=" + id;
		LdapTesting ldapTesting = restTemplate.getForObject(url, LdapTesting.class);
		return ldapTesting;
	}
	
	public Map<String, String> verifyUserPassword(String operation, String id, String password) {
		String url = "http://10.14.18.177:5623/ldapService?operation=" + operation +"&id="+ id + "&password=" + password;
		Map<String, String> mapBody = (Map<String, String>) restTemplate.getForObject(url, Map.class);
		return mapBody;
	}
}
