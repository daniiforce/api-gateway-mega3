package com.mega.gateway.service;

import java.util.List;

import org.springframework.stereotype.Component;

import com.mega.gateway.model.DataType;

@Component
public interface DataTypeInterfaceService {
	
	List<DataType> findAll();
	
	DataType findByCode(String code);
}
