package com.mega.gateway.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mega.gateway.model.Reward;
import com.mega.gateway.repository.RewardRepository;
import com.mega.gateway.service.RewardInterfaceService;

@Service
public class RewardServiceImpl implements RewardInterfaceService {

	@Autowired
	private RewardRepository rewardRepository;

	@Override
	public Reward save(Reward reward) {
		return rewardRepository.save(reward);
	}

	@Override
	public Reward findByEmpId(String empId) {
		return rewardRepository.findByEmpId(empId);
	}

	@Override
	public Reward findByEmpIdAndStartDateAndEndDateAndSeq(String empId, String startDate, String endDate, String seq) {
		return rewardRepository.findByEmpIdAndStartDateAndEndDateAndSeq(empId, startDate, endDate, seq);
	}

	@Override
	public List<Reward> findByEmpIdEquals(String empId) {
		return rewardRepository.findByEmpIdEquals(empId);
	}

}
