package com.mega.gateway.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mega.gateway.model.Province;
import com.mega.gateway.repository.ProvinceRepository;
import com.mega.gateway.service.ProvinceInterfaceService;

@Service
public class ProvinceServiceImpl implements ProvinceInterfaceService {

	@Autowired
	private ProvinceRepository provinceRepository;

	@Override
	public List<Province> findAll() {
		return provinceRepository.findAll();
	}

	@Override
	public Province findByCode(String code) {
		return provinceRepository.findByCode(code);
	}

}
