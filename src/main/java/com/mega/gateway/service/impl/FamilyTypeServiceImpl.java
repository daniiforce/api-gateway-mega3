package com.mega.gateway.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mega.gateway.model.FamilyType;
import com.mega.gateway.repository.FamilyTypeRepository;
import com.mega.gateway.service.FamilyTypeInterfaceService;

@Service
public class FamilyTypeServiceImpl implements FamilyTypeInterfaceService {

	@Autowired
	private FamilyTypeRepository familyTypeRepository;

	@Override
	public List<FamilyType> findAll() {
		return familyTypeRepository.findAll();
	}

}
