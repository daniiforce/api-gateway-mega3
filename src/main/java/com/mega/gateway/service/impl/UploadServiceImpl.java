//package com.mega.gateway.service.impl;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import com.mega.gateway.model.Upload;
//import com.mega.gateway.repository.UploadRepository;
//import com.mega.gateway.service.UploadService;
//
//@Service
//public class UploadServiceImpl implements UploadService{
//	
//	@Autowired
//	private UploadRepository uploadRepository;
//
//	@Override
//	public Upload save(Upload upload) {
//		return uploadRepository.save(upload);
//	}
//
//	@Override
//	public Upload findByName(String name) {
//		return uploadRepository.findByName(name);
//	}
//
//}
