package com.mega.gateway.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mega.gateway.model.LastEducation;
import com.mega.gateway.repository.LastEducationRepository;
import com.mega.gateway.service.LastEducationInterfaceService;

@Service
public class LastEducationServiceImpl implements LastEducationInterfaceService {

	@Autowired
	private LastEducationRepository lastEducationRepository;

	@Override
	public List<LastEducation> findAll() {
		return lastEducationRepository.findAll();
	}

}
