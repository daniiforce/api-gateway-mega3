package com.mega.gateway.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mega.gateway.model.Address;
import com.mega.gateway.repository.AddressRepository;
import com.mega.gateway.service.AddressInterfaceService;

@Service
public class AddressServiceImpl implements AddressInterfaceService{
	
	@Autowired
	private AddressRepository addressRepository;

	@Override
	public Address save(Address address) {
		return addressRepository.save(address);
	}

	@Override
	public Address findByEmpId(String empId) {
		return addressRepository.findByEmpId(empId);
	}

	@Override
	public List<Address> findAll() {
		return addressRepository.findAll();
	}

	@Override
	public Address findByEmpIdAndStartDateAndEndDateAndAddressTypeAndSeq(String empId, String startDate,
			String endDate, String addressType, String seq) {
		return addressRepository.findByEmpIdAndStartDateAndEndDateAndAddressTypeAndSeq(empId, startDate, endDate, addressType, seq);
	}

	@Override
	public List<Address> findByEmpIdEquals(String empId) {
		return addressRepository.findByEmpIdEquals(empId);
	}

}
