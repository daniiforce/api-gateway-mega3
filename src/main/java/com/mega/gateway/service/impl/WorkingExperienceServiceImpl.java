package com.mega.gateway.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mega.gateway.model.WorkingExperience;
import com.mega.gateway.repository.WorkingExperienceRepository;
import com.mega.gateway.service.WorkingExperienceInterfaceService;

@Service
public class WorkingExperienceServiceImpl implements WorkingExperienceInterfaceService {

	@Autowired
	private WorkingExperienceRepository workingExperienceRepository;

	@Override
	public WorkingExperience save(WorkingExperience workingExperience) {
		return workingExperienceRepository.save(workingExperience);
	}

	@Override
	public WorkingExperience findByEmpId(String empId) {
		return workingExperienceRepository.findByEmpId(empId);
	}

	@Override
	public WorkingExperience findByEmpIdAndStartDateAndEndDateAndSeq(String empId, String startDate, String endDate,
			String seq) {
		return workingExperienceRepository.findByEmpIdAndStartDateAndEndDateAndSeq(empId, startDate, endDate, seq);
	}

	@Override
	public List<WorkingExperience> findByEmpIdEquals(String empId) {
		return workingExperienceRepository.findByEmpIdEquals(empId);
	}

}
