package com.mega.gateway.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mega.gateway.model.WorkIndustry;
import com.mega.gateway.repository.WorkIndustryRepository;
import com.mega.gateway.service.WorkIndustryInterfaceService;

@Service
public class WorkIndustryServiceImpl implements WorkIndustryInterfaceService {

	@Autowired
	private WorkIndustryRepository workIndustryRepository;

	@Override
	public List<WorkIndustry> findAll() {
		return workIndustryRepository.findAll();
	}

	@Override
	public WorkIndustry findByCode(String code) {
		return workIndustryRepository.findByCode(code);
	}

}
