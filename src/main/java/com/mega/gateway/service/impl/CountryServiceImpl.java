package com.mega.gateway.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mega.gateway.model.Country;
import com.mega.gateway.repository.CountryRepository;
import com.mega.gateway.service.CountryInterfaceService;

@Service
public class CountryServiceImpl implements CountryInterfaceService {

	@Autowired
	private CountryRepository countryRepository;

	@Override
	public List<Country> findAll() {
		return countryRepository.findAll();
	}

	@Override
	public Country findByCountryId(String countryId) {
		return countryRepository.findByCountryId(countryId);
	}

}
