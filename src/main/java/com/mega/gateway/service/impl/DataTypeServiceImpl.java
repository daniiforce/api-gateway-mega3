package com.mega.gateway.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mega.gateway.model.DataType;
import com.mega.gateway.repository.DataTypeRepository;
import com.mega.gateway.service.DataTypeInterfaceService;

@Service
public class DataTypeServiceImpl implements DataTypeInterfaceService {

	@Autowired
	private DataTypeRepository dataTypeRepository;

	@Override
	public List<DataType> findAll() {
		return dataTypeRepository.findAll();
	}

	@Override
	public DataType findByCode(String code) {
		return dataTypeRepository.findByCode(code);
	}

}
