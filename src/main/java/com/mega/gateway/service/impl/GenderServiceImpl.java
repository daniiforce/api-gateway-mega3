package com.mega.gateway.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mega.gateway.model.Gender;
import com.mega.gateway.repository.GenderRepository;
import com.mega.gateway.service.GenderInterfaceService;

@Service
public class GenderServiceImpl implements GenderInterfaceService {

	@Autowired
	private GenderRepository genderRepository;

	@Override
	public List<Gender> findAll() {
		return genderRepository.findAll();
	}

}
