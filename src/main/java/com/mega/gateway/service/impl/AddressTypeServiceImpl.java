package com.mega.gateway.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mega.gateway.model.AddressType;
import com.mega.gateway.repository.AddressTypeRepository;
import com.mega.gateway.service.AddressTypeInterfaceService;

@Service
public class AddressTypeServiceImpl implements AddressTypeInterfaceService{

	@Autowired
	private AddressTypeRepository addressTypeRepository;
	
	@Override
	public List<AddressType> findAll() {
		return addressTypeRepository.findAll();
	}

	@Override
	public AddressType findByCode(String code) {
		return addressTypeRepository.findByCode(code);
	}

}
