package com.mega.gateway.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mega.gateway.model.Family;
import com.mega.gateway.repository.FamilyRepository;
import com.mega.gateway.service.FamilyInterfaceService;

@Service
public class FamilyServiceImpl implements FamilyInterfaceService {

	@Autowired
	private FamilyRepository familyRepository;

	@Override
	public Family save(Family family) {
		return familyRepository.save(family);
	}

	@Override
	public Family findByEmpId(String empId) {
		return familyRepository.findByEmpId(empId);
	}

}
