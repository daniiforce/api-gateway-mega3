//package com.mega.gateway.service.impl;
//
//import java.util.Optional;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import com.mega.gateway.model.User;
//import com.mega.gateway.repository.UserRepository;
//import com.mega.gateway.service.UserService;
//
//@Service
//public class UserServiceImpl implements UserService{
//	
//	@Autowired
//	private UserRepository userRepository;
//
//	@Override
//	public Optional<User> findByUsernameOption(String username) {
//		return userRepository.findByUsername(username);
//	}
//
//	@Override
//	public Optional<User> findByUsername(String username) {
//		return userRepository.findByUsernameIgnoreCase(username);
//	}
//
//}
