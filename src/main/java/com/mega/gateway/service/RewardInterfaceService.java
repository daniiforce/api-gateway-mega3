package com.mega.gateway.service;

import java.util.List;

import org.springframework.stereotype.Component;

import com.mega.gateway.model.Reward;

@Component
public interface RewardInterfaceService {
	
	Reward save(Reward reward);
	
	Reward findByEmpId(String empId);
	
	Reward findByEmpIdAndStartDateAndEndDateAndSeq(String empId, String startDate, String endDate, String seq);
	
	List<Reward> findByEmpIdEquals(String empId);
}
