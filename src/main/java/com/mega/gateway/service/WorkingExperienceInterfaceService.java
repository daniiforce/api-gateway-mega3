package com.mega.gateway.service;

import java.util.List;

import org.springframework.stereotype.Component;

import com.mega.gateway.model.WorkingExperience;

@Component
public interface WorkingExperienceInterfaceService {
	
	WorkingExperience save(WorkingExperience workingExperience);
	
	WorkingExperience findByEmpId(String empId);
	
	WorkingExperience findByEmpIdAndStartDateAndEndDateAndSeq(String empId, String startDate, String endDate, String seq);
	
	List<WorkingExperience> findByEmpIdEquals(String empId);
}
