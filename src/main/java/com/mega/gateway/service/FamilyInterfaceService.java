package com.mega.gateway.service;

import org.springframework.stereotype.Component;

import com.mega.gateway.model.Family;

@Component
public interface FamilyInterfaceService {
	
	Family save(Family family);
	
	Family findByEmpId(String empId);
}
