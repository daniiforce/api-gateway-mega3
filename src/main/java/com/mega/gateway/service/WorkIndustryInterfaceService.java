package com.mega.gateway.service;

import java.util.List;

import org.springframework.stereotype.Component;

import com.mega.gateway.model.WorkIndustry;

@Component
public interface WorkIndustryInterfaceService {
	
	List<WorkIndustry> findAll();
	
	WorkIndustry findByCode(String code);
}
