package com.mega.gateway.service;

import java.util.List;

import org.springframework.stereotype.Component;

import com.mega.gateway.model.Country;

@Component
public interface CountryInterfaceService {
	
	List<Country> findAll();
	
	Country findByCountryId(String countryId);
}
