package com.mega.gateway.service;

import java.util.List;

import org.springframework.stereotype.Component;

import com.mega.gateway.model.AddressType;

@Component
public interface AddressTypeInterfaceService {
	
	List<AddressType> findAll();
	
	AddressType findByCode(String code);
}
