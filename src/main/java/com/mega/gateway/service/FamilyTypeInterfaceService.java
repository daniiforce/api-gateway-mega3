package com.mega.gateway.service;

import java.util.List;

import org.springframework.stereotype.Component;

import com.mega.gateway.model.FamilyType;

@Component
public interface FamilyTypeInterfaceService {
	
	List<FamilyType> findAll();
}
