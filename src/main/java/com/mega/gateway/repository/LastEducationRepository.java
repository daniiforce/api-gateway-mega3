package com.mega.gateway.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mega.gateway.model.LastEducation;


public interface LastEducationRepository extends JpaRepository<LastEducation, Long>{
	
}
