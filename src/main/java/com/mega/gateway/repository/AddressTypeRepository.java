package com.mega.gateway.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mega.gateway.model.AddressType;

public interface AddressTypeRepository extends JpaRepository<AddressType, Long>{
	AddressType findByCode(String code);
}
