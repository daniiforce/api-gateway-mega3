package com.mega.gateway.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mega.gateway.model.WorkingExperience;

public interface WorkingExperienceRepository extends JpaRepository<WorkingExperience, Long>{
	
	WorkingExperience findByEmpId(String empId);
	
	WorkingExperience findByEmpIdAndStartDateAndEndDateAndSeq(String empId, String startDate, String endDate, String seq);
	
	List<WorkingExperience> findByEmpIdEquals(String empId);
}
