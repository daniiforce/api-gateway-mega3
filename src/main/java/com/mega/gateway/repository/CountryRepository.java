package com.mega.gateway.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mega.gateway.model.Country;

public interface CountryRepository extends JpaRepository<Country, Long>{
	Country findByCountryId(String countryId);
}
