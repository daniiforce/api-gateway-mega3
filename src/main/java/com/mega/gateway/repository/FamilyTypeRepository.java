package com.mega.gateway.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mega.gateway.model.FamilyType;

public interface FamilyTypeRepository extends JpaRepository<FamilyType, Long>{
	
}
