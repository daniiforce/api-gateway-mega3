package com.mega.gateway.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mega.gateway.model.Province;

public interface ProvinceRepository extends JpaRepository<Province, Long>{
	Province findByCode(String code);
}
