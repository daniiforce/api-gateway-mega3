package com.mega.gateway.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mega.gateway.model.WorkIndustry;


public interface WorkIndustryRepository extends JpaRepository<WorkIndustry, Long>{
	WorkIndustry findByCode(String code);
}
