package com.mega.gateway.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mega.gateway.model.Gender;

public interface GenderRepository extends JpaRepository<Gender, Long>{
	
}
