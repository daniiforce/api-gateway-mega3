package com.mega.gateway.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mega.gateway.model.Address;

public interface AddressRepository extends JpaRepository<Address, Long>{
	
	Address findByEmpId(String empId);
	
	List<Address> findByEmpIdEquals(String empId);
	
	Address findByEmpIdAndStartDateAndEndDateAndAddressTypeAndSeq(String empId, String startDate, String endDate, String addressType, String seq);
}
