//package com.mega.gateway.repository;
//
//import java.util.Optional;
//
//import org.springframework.data.jpa.repository.JpaRepository;
//
//import com.mega.gateway.model.User;
//
//public interface UserRepository extends JpaRepository<User, Long>{
//	Optional<User> findByUsernameIgnoreCase(String username);
//	
//	Optional<User> findByUsername(String username);
//}
