package com.mega.gateway.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mega.gateway.model.DataType;

public interface DataTypeRepository extends JpaRepository<DataType, Long>{
	DataType findByCode(String code);
}
