package com.mega.gateway.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mega.gateway.model.Family;

public interface FamilyRepository extends JpaRepository<Family, Long>{
	
	Family findByEmpId(String empId);
}
