package com.mega.gateway.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mega.gateway.model.Reward;

public interface RewardRepository extends JpaRepository<Reward, Long>{
	
	Reward findByEmpId(String empId);
	
	Reward findByEmpIdAndStartDateAndEndDateAndSeq(String empId, String startDate, String endDate, String seq);
	
	List<Reward> findByEmpIdEquals(String empId);
}
