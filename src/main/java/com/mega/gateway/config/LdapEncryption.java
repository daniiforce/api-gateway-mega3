package com.mega.gateway.config;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.security.crypto.password.PasswordEncoder;

public class LdapEncryption implements PasswordEncoder {
	private final Log logger = LogFactory.getLog(getClass());

//	LdapSalt = gek123
//	ldapSecret = iknowwhatyoudidlastnigth
//
//	encPassword = LdapEncryptPassword.encrypt(ldapSalt, password, ldapSecret);
	
//	private String feedbackEncryption;
//	
//	public String getFeedbackEncryption() {
//		return feedbackEncryption;
//	}
//
//	public void setFeedbackEncryption(String feedbackEncryption) {
//		this.feedbackEncryption = feedbackEncryption;
//	}
//
//	public LdapEncryption() {
//		
//	}
//
//	public LdapEncryption(String salt,String password,String secret) {
//		try {
//			String feedbackEncryption = encrypt(salt, password, secret);
//			this.feedbackEncryption = feedbackEncryption;
//		} catch (NoSuchAlgorithmException e) {
//			e.printStackTrace();
//		}
//	}

	public static String encrypt(String salt,String password,String secret) throws NoSuchAlgorithmException {
		
		if(salt.length() > 2) {
	        salt = salt.substring(0, 2);	
		}
		String md5_text = salt + secret;

		MessageDigest md = MessageDigest.getInstance("MD5");
		md.update(md5_text.getBytes());
		byte[] digest = md.digest();
		byte[] bsalt = salt.getBytes();
		byte[] pwd = password.getBytes();
		

		int length  = salt.length() + (digest.length * (pwd.length + 16) / 16);	
		
		int i = 0,p = 0,j = 0;
		byte[] result = new byte[99999];
		
		for(;i<bsalt.length;i++) {
			result[i] = bsalt[i];
		}
		for(;i<length;i++) {
			if(p < pwd.length) {
				result[i] = (byte) (pwd[p] ^ digest[(j%digest.length)]);
			}else {
				result[i] = (byte) (0^digest[(j%digest.length)]);
			}
			j++;
			p++;
		}
		String encPassword = Base64.getEncoder().withoutPadding().encodeToString(trim(result));
		return encPassword;
	}
	
	static byte[] trim(byte[] bytes)
	{
	    int i = bytes.length - 1;
	    while (i >= 0 && bytes[i] == 0)
	    {
	        --i;
	    }

	    return Arrays.copyOf(bytes, i + 1);
	}

	@Override
	public String encode(CharSequence rawPassword) {
		String feedbackEncryption = "";
		try {
			feedbackEncryption = encrypt("gek123", rawPassword.toString(), "iknowwhatyoudidlastnigth");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return feedbackEncryption;
	}

	@Override
	public boolean matches(CharSequence rawPassword, String encodedPassword) {
		boolean temp = false;
		if (encodedPassword.equalsIgnoreCase("") || encodedPassword == null || encodedPassword.length() == 0) {
			logger.warn("Empty encoded password");
			temp = false;
			return temp;
		}
		
		String feedbackEncryption = "";
		try {
			feedbackEncryption = encrypt("gek123", rawPassword.toString(), "iknowwhatyoudidlastnigth");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		
		if(!feedbackEncryption.equalsIgnoreCase("") && feedbackEncryption != null) {
			if(feedbackEncryption.equalsIgnoreCase(encodedPassword)) {
				temp = true;
			}
		}
		return temp;
	}
}